report 50124 "RSMUSAgedInventory"
{
    UsageCategory = ReportsAndAnalysis;
    ApplicationArea = All;
    Caption = 'ITRenew - Aged Inventory';
    RDLCLayout = './Report Layouts/Rep50124.RSMUSAgedInventory.rdl';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(Item; Item)
        {
            column(TotalQty2; TotalQty2)
            { }
            column(LastNo; LastNo)
            { }
            column(QuantityGeneral; QuantityGeneral)
            { }
            column(Vartext; Vartext)
            {
            }
            column(Item_No; "No.")
            {

            }
            column(Description; Description)
            {

            }
            column(Item_Category_Code; "Item Category Code")
            {

            }
            column(DateRun; Format(DateRun, 0, 4))
            { }
            column(TotalQty; TotalQty)
            { }
            column(UnitCost; UnitCost)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(TotalValue; TotalValue)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(QtyCurrent; QtyCurrent)
            {
            }
            column(CostCurrent; CostCurrent)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(Qty90Days; Qty90Days)
            {
            }
            column(Cost90Days; Cost90Days)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(Qty180Days; Qty180Days)
            {
            }
            column(Cost180Days; Cost180Days)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(Qty270Days; Qty270Days)
            {
            }
            column(Cost270Days; Cost270Days)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(Qty360Days; Qty360Days)
            {
            }
            column(Cost360Days; Cost360Days)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(Qty360PlusDays; Qty360PlusDays)
            {
            }
            column(Cost360PlusDays; Cost360PlusDays)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(QtyUnused; QtyUnused)
            { }

            column(CostUnused; CostUnused)
            {
                AutoFormatType = 10;
                AutoFormatExpression = '1,USD';
            }
            column(QtyCurrentLbl; QtyCurrentLbl)
            { }
            column(CostCurrentLbl; CostCurrentLbl)
            { }
            column(Qty90Lbl; Qty90Lbl)
            { }
            column(Cost90Lbl; Cost90Lbl)
            { }
            column(Qty180Lbl; Qty180Lbl)
            { }
            column(Cost180Lbl; Cost180Lbl)
            { }
            column(Qty270Lbl; Qty270Lbl)
            { }
            column(Cost270Lbl; Cost270Lbl)
            { }
            column(Qty360Lbl; Qty360Lbl)
            { }
            column(Cost360Lbl; Cost360Lbl)
            { }
            column(Qty360PlusLbl; Qty360PlusLbl)
            { }
            column(Cost360PlusLbl; Cost360PlusLbl)
            { }
            column(HeaderLbl; HeaderLbl)
            { }
            column(RunDateLbl; RunDateLbl)
            { }
            column(ItemFilterLbl; ItemFilterLbl)
            { }
            column(ItemNoLbl; ItemNoLbl)
            { }
            column(DescriptionLbl; DescriptionLbl)
            { }
            column(ItemCategoryLbl; ItemCategoryLbl)
            { }

            trigger OnPreDataItem()
            begin
                Vartext := Item.GetFilters;
            end;

            trigger OnAfterGetRecord()
            var
                ItemLedgerEntry: Record "Item Ledger Entry";
                ItemLedgerEntry2: Record "Item Ledger Entry";
            begin
                TotalQty := 0;
                TotalQty2 := 0;
                QtyCurrent := 0;
                CostCurrent := 0;
                Qty90Days := 0;
                Cost90Days := 0;
                Qty180Days := 0;
                Cost180Days := 0;
                Qty270Days := 0;
                Cost270Days := 0;
                Qty360Days := 0;
                Cost360Days := 0;
                Qty360PlusDays := 0;
                Cost360PlusDays := 0;

                ItemLedgerEntry.SetRange("Item No.", Item."No.");
                //ItemLedgerEntry.SetLoadFields(ItemLedgerEntry.Quantity, ItemLedgerEntry."Cost Amount (Actual)");
                if ItemLedgerEntry.Find('-') then
                    repeat
                        if (ItemLedgerEntry."Posting Date" <= DateRun) then
                            TotalQty += ItemLedgerEntry.Quantity;
                    until ItemLedgerEntry.Next() = 0;

                Qty90Days := 0;
                Cost90Days := 0;
                Qty180Days := 0;
                Cost180Days := 0;
                Qty270Days := 0;
                Cost270Days := 0;
                Qty360Days := 0;
                Cost360Days := 0;
                Qty360PlusDays := 0;
                Cost360PlusDays := 0;
                ExitVar := false;

                ItemLedgerEntry2.SetRange("Item No.", Item."No.");
                //ItemLedgerEntry2.SetLoadFields(ItemLedgerEntry.Quantity, ItemLedgerEntry."Cost Amount (Actual)");
                if ItemLedgerEntry2.FindLast() then
                    repeat
                        if (ItemLedgerEntry2.Quantity > 0) AND (ItemLedgerEntry2."Posting Date" <= DateRun) then begin
                            ItemLedgerEntry2.CalcFields("Cost Amount (Actual)");
                            QuantityGeneral := ItemLedgerEntry2.Quantity;
                            CostGeneral := ItemLedgerEntry2."Cost Amount (Actual)";

                            if ItemLedgerEntry2.Quantity + TotalQty2 >= TotalQty then begin
                                QuantityGeneral := TotalQty - TotalQty2;
                                if (ItemLedgerEntry2.Quantity <> 0) then
                                    CostGeneral := QuantityGeneral * (ItemLedgerEntry2."Cost Amount (Actual)" / ItemLedgerEntry2.Quantity);
                                LastNo := ItemLedgerEntry2."Document No.";
                                ExitVar := true;
                            end;

                            case true of
                                ItemLedgerEntry2."Posting Date" >= Date90:
                                    begin
                                        Qty90Days += QuantityGeneral;
                                        TotalQty2 += QuantityGeneral;
                                        Cost90Days += CostGeneral;
                                    end;
                                ItemLedgerEntry2."Posting Date" >= Date180:
                                    begin
                                        Qty180Days += QuantityGeneral;
                                        TotalQty2 += QuantityGeneral;
                                        Cost180Days += CostGeneral;
                                    end;
                                ItemLedgerEntry2."Posting Date" >= Date270:
                                    begin
                                        Qty270Days += QuantityGeneral;
                                        TotalQty2 += QuantityGeneral;
                                        Cost270Days += CostGeneral;
                                    end;
                                ItemLedgerEntry2."Posting Date" >= Date360:
                                    begin
                                        Qty360Days += QuantityGeneral;
                                        TotalQty2 += QuantityGeneral;
                                        Cost360Days += CostGeneral;
                                    end;
                                ItemLedgerEntry2."Posting Date" < Date360:
                                    begin
                                        Qty360PlusDays += QuantityGeneral;
                                        TotalQty2 += QuantityGeneral;
                                        Cost360PlusDays += CostGeneral;
                                    end;
                                else
                                    QtyUnused += QuantityGeneral;
                                    CostUnused += CostGeneral;
                            end;
                        end;
                    until (ItemLedgerEntry2.Next(-1) = 0) OR (ExitVar);

                TotalValue := Cost90Days + Cost180Days + Cost270Days + Cost360Days + Cost360PlusDays;
                if TotalQty = 0 then
                    UnitCost := 0
                else
                    UnitCost := TotalValue / TotalQty;
            end;

        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    Caption = 'Date';
                    field(DateRun; DateRun)
                    {
                        ApplicationArea = Basic, Suite;
                        Caption = 'Date';
                        ToolTip = 'Specify the date to run the report as of. If left empty will default to today.';
                    }
                }
            }
        }
    }

    trigger OnPreReport()
    begin
        if DateRun = 0D then
            DateRun := Today;
        Date90 := DateRun - 90;
        Date180 := DateRun - 180;
        Date270 := DateRun - 270;
        Date360 := DateRun - 360;
    end;

    var
        LastNo: Code[20];
        QuantityGeneral: Decimal;
        CostGeneral: Decimal;
        OverAmount: Decimal;
        ExitVar: Boolean;
        TotalQty2: Decimal;
        Vartext: Text[2048];
        DateRun: Date;
        Date90: Date;
        Date180: Date;
        Date270: Date;
        Date360: Date;
        TotalQty: Decimal;
        UnitCost: Decimal;
        TotalValue: Decimal;
        QtyCurrent: Decimal;
        CostCurrent: Decimal;
        Qty90Days: Decimal;
        Cost90Days: Decimal;
        Qty180Days: Decimal;
        Cost180Days: Decimal;
        Qty270Days: Decimal;
        Cost270Days: Decimal;
        Qty360Days: Decimal;
        Cost360Days: Decimal;
        Qty360PlusDays: Decimal;
        Cost360PlusDays: Decimal;
        Qty90Total: Decimal;
        Cost90Total: Decimal;
        Qty180Total: Decimal;
        Cost180Total: Decimal;
        Qty270Total: Decimal;
        Cost270Total: Decimal;
        Qty360Total: Decimal;
        Cost360Total: Decimal;
        Qty360PlusTotal: Decimal;
        Cost360PlusTotal: Decimal;
        QtyUnused: Decimal;
        CostUnused: Decimal;
        QtyCurrentLbl: Label 'Current Qty.';
        CostCurrentLbl: Label 'Current Cost';
        Qty90Lbl: Label '90 Day Qty.';
        Cost90Lbl: Label '90 Day Cost';
        Qty180Lbl: Label '180 Day Qty.';
        Cost180Lbl: Label '180 Day Cost';
        Qty270Lbl: Label '270 Day Qty.';
        Cost270Lbl: Label '270 Day Cost';
        Qty360Lbl: Label '360 Day Qty.';
        Cost360Lbl: Label '360 Day Cost';
        Qty360PlusLbl: Label '360+ Day Qty.';
        Cost360PlusLbl: Label '360+ Day Cost';
        HeaderLbl: Label 'Aged Inventory';
        RunDateLbl: Label 'Run Date: ';
        ItemFilterLbl: Label 'Item Filters: ';
        ItemNoLbl: Label 'Item No.';
        DescriptionLbl: Label 'Description';
        ItemCategoryLbl: Label 'Item Category';

}