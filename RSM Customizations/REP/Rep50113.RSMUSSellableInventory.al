report 50113 "RSMUSSellableInventory" // 50113
{
    DefaultLayout = RDLC;
    RDLCLayout = './Report Layouts/Rep50113.RSMUSSellableInventory.rdl';
    Caption = 'Sellable Inventory by Location and Variant';
    ApplicationArea = All;
    UsageCategory = ReportsAndAnalysis;
    Permissions = tabledata RSMUSLocationItemVariantBuffer = rimd;

    dataset
    {
        dataitem(Buffer; RSMUSLocationItemVariantBuffer)
        {
            UseTemporary = true;
            RequestFilterFields = "Location Code", "Item No.", "Item Description", "Variant Code", "Variant Description 2";
            column(ItemNo_Buffer; Buffer."Item No.")
            {
            }
            column(VariantCode_Buffer; Buffer."Variant Code")
            {
            }
            column(LocationCode_Buffer; Buffer."Location Code")
            {
            }
            column(Label_Buffer; Buffer.Label)
            {
            }
            column(RemainingQuantity_Buffer; Buffer."Remaining Quantity")
            {
            }
            column(RSMUSItemDescription; "Item Description")
            {
                Caption = 'Item Description';
            }
            column(RSMUSVariantDescription; "Variant Description 2")
            {
                Caption = 'Variant Description';
            }

            dataitem(Integer; Integer)
            {
                DataItemTableView = sorting(Number) where(Number = const(1));
                column(CompanyName; companyproperty.DisplayName())
                {

                }
                column(TodayFormatted; format(Today, 0, 4))
                {

                }
                column(SellableInventoryListCaption; SellableInventoryListCaptionLbl)
                {

                }
            }
            trigger OnPreDataItem()
            begin
                if IsEmpty then error('Nothing to show.');
            end;

            trigger OnAfterGetRecord()
            begin
                CalcFields("Item Description", "Variant Description 2");
            end;
        }
    }

    requestpage
    {
        Caption = 'Sellable Inventory - Filters';
        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPreReport()
    begin
        if buffer.IsEmpty then
            PopulateBuffer(buffer);
    end;

    procedure PopulateBuffer(var Buffer: Record RSMUSLocationItemVariantBuffer)
    var
        InventoryReporting: codeunit RSMUSInventoryReporting;
    begin
        InventoryReporting.PopulateLocItemVarBuffer(Buffer);
        if not buffer.isempty then
            buffer.findfirst;
    end;

    var
        SellableInventoryListCaptionLbl: Label 'Sellable Inventory List';
}

