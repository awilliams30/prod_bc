report 50123 "CalculateSalesPriceCalRules"
{
    Caption = 'Calc. Sales Price Rules';
    UsageCategory = Administration;
    ApplicationArea = All;
    ProcessingOnly = true;
    AdditionalSearchTerms = 'Calculate Sales Price Rules';
    dataset
    {
        dataitem(Item; Item)
        {
            RequestFilterFields = "No.";
            trigger OnAfterGetRecord()
            begin
                CalculateSalesPriceRule();
            end;

        }
    }
    requestpage
    {
        SaveValues = true;
        layout
        {
            area(Content)
            {
                group(Options)
                {
                    Caption = 'Options';
                    field(CalculationPeriod; CalculationPeriod)
                    {
                        ApplicationArea = All;

                    }
                }
            }
        }

        actions
        {
            area(processing)
            {
                action(ActionName)
                {
                    ApplicationArea = All;

                }
            }
        }
    }
    trigger OnInitReport()
    begin
        SalesSetup.get;
        TempSalesPriceCalculationRule.DeleteAll();
        if format(CalculationPeriod) = '' then
            CalculationPeriod := SalesSetup.RSMUSSalesPriceRulePeriod;


    end;

    trigger OnPreReport()
    var
        filtes: text[100];
    begin
        if NOT SalesSetup."RSMUSSales Price Rules Enabled" then
            CurrReport.Break();
        if format(SalesSetup.RSMUSSalesPriceRulePeriod) = '' then
            CurrReport.Break();
    end;

    trigger OnPostReport()
    begin
        CreateUpdateSalesPriceRule();
        Message('Done');
    end;

    local procedure CreateUpdateSalesPriceRule()
    begin
        if TempSalesPriceCalculationRule.FindSet() then
            repeat
                if TempSalesPriceCalculationRule.Quantity <> 0 then
                    TempSalesPriceCalculationRule."Average Selling Price" := TempSalesPriceCalculationRule.Amount / TempSalesPriceCalculationRule.Quantity;
                if not SalesPriceCalculationRule.Get(TempSalesPriceCalculationRule."Item No.", TempSalesPriceCalculationRule."Variant Code") then begin
                    SalesPriceCalculationRule := TempSalesPriceCalculationRule;
                    SalesPriceCalculationRule.Insert();
                end else begin
                    SalesPriceCalculationRule.Quantity := TempSalesPriceCalculationRule.Quantity;
                    SalesPriceCalculationRule.Amount := round(TempSalesPriceCalculationRule.Amount, 0.01);
                    SalesPriceCalculationRule."Average Selling Price" := TempSalesPriceCalculationRule."Average Selling Price";
                    SalesPriceCalculationRule."Last Date/Time Modified" := TempSalesPriceCalculationRule."Last Date/Time Modified";
                    SalesPriceCalculationRule.Modify();
                end;
            until TempSalesPriceCalculationRule.Next() = 0;
    end;

    local procedure CalculateSalesPriceRule()
    var
        Quantity: Decimal;
        Amount: Decimal;
        AverageSellingPrice: Decimal;

    begin
        Quantity := 0;
        Amount := 0;
        ItemLedgerEntry.SetCurrentKey("Item No.", "Entry Type", "Variant Code", "Drop Shipment", "Location Code", "Posting Date");
        ItemLedgerEntry.SetRange("Item No.", Item."No.");
        ItemLedgerEntry.SetFilter("Posting Date", '>=%1', CalcDate(CalculationPeriod, Today));
        ItemLedgerEntry.Setrange("Entry Type", ItemLedgerEntry."Entry Type"::Sale);
        ItemLedgerEntry.SetAutoCalcFields("Sales Amount (Actual)", "Cost Amount (Expected)");
        IF ItemLedgerEntry.findset then
            repeat
                if ItemLedgerEntry."Document Type" = ItemLedgerEntry."Document Type"::"Sales Shipment" then
                    if not TempSalesPriceCalculationRule.Get(Item."No.", ItemLedgerEntry."Variant Code") then begin
                        TempSalesPriceCalculationRule.Init();
                        TempSalesPriceCalculationRule."Item No." := item."No.";
                        TempSalesPriceCalculationRule."Variant Code" := ItemLedgerEntry."Variant Code";
                        TempSalesPriceCalculationRule.Quantity := ItemLedgerEntry."Invoiced Quantity" * -1;
                        TempSalesPriceCalculationRule.Amount := ItemLedgerEntry."Sales Amount (Actual)";
                        TempSalesPriceCalculationRule."Last Date/Time Modified" := CurrentDateTime();
                        TempSalesPriceCalculationRule.Insert();
                    end else begin
                        TempSalesPriceCalculationRule.Quantity += ItemLedgerEntry."Invoiced Quantity" * -1;
                        TempSalesPriceCalculationRule.Amount += ItemLedgerEntry."Sales Amount (Actual)";
                        TempSalesPriceCalculationRule."Last Date/Time Modified" := CurrentDateTime();
                        TempSalesPriceCalculationRule.Modify();
                    end;
            until ItemLedgerEntry.next = 0;
    end;


    var
        CalculationPeriod: DateFormula;
        SalesPriceCalculationRule: Record RSMUSSalesPriceCalculationRule;
        TempSalesPriceCalculationRule: Record RSMUSSalesPriceCalculationRule temporary;
        SalesSetup: Record "Sales & Receivables Setup";
        ItemLedgerEntry: record "Item Ledger Entry";

}