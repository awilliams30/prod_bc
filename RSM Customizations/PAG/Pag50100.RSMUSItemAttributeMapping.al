page 50100 "RSMUSItemAttributeMapping"
{
    Caption = 'Item Attribute Value Mapping';
    PageType = List;
    UsageCategory = Lists;
    SourceTable = "Item Attribute Value Mapping";
    //ApplicationArea = all;
    Editable = true;

    layout
    {
        area(Content)
        {
            repeater(AttributeMappingList)
            {
                field("Table ID"; "Table ID")
                {
                    ApplicationArea = All;
                    Editable = true;
                }
                field("No."; "No.")
                {
                    ApplicationArea = All;
                    TableRelation = Item."No.";
                    DrillDown = true;

                }
                field("Item Attribute ID"; "Item Attribute ID")
                {
                    ApplicationArea = All;
                    Editable = true;
                }
                // field(ItemAttribute; RSMUSAttributeName)
                // {
                //     ApplicationArea = All;
                //     TableRelation = "Item Attribute".Name;
                //     DrillDown = true;
                // }
                field("Item Attribute Value ID"; "Item Attribute Value ID")
                {
                    ApplicationArea = All;
                    Editable = true;
                    TableRelation = "Item Attribute Value" where("Attribute ID" = field("Item Attribute ID"));
                    DrillDown = true;
                    DrillDownPageId = "Item Attribute Values";

                }
                // field(ItemAttributeVal; RSMUSAttributeValName)
                // {
                //     ApplicationArea = All;
                //     TableRelation = "Item Attribute Value"."Attribute Name" where("Attribute ID" = field("Item Attribute ID"));
                //     DrillDown = true;
                // }
            }
        }
    }
    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        if "Table ID" = 0 then
            "Table ID" := 27;
    end;

    var
        RunPageUpdate: Boolean;
}