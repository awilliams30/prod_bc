page 50103 "RSMUSAllItemVariants"
{
    PageType = List;
    ApplicationArea = none;
    UsageCategory = Administration;
    SourceTable = RSMUSAllItemVariants;
    Editable = false;
    Permissions = tabledata "RSMUSAllItemVariants" = rmid;

    layout
    {
        area(Content)
        {
            repeater(AllVariants)
            {
                field("Item No.";
                "Item No.")
                {
                    ApplicationArea = All;
                }
                field("Code"; Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = all;
                }
                field("Description 2"; "Description 2")
                {
                    ApplicationArea = all;
                }
            }
        }
    }
}