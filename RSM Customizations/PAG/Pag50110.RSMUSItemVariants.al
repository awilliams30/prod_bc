page 50110 "RSMUSItemVariants"
{
    PageType = List;
    ApplicationArea = none;
    UsageCategory = Administration;
    SourceTable = RSMUSItemVariants;
    Editable = false;
    Permissions = tabledata "RSMUSItemVariants" = rmid;

    layout
    {
        area(Content)
        {
            repeater(AllVariants)
            {
                field("Item No.";
                "Item No.")
                {
                    ApplicationArea = All;
                }
                field("Code"; Code)
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = all;
                }
                field("Description 2"; "Description 2")
                {
                    ApplicationArea = all;
                }
            }
        }
    }
}