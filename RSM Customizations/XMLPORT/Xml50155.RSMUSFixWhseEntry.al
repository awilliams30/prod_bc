xmlport 50155 "RSMUSFixWhseEntry" // 50155
{
    Format = VariableText;
    Direction = Import;
    Caption = 'RSM Fix Warehouse Entry';
    Permissions = tabledata 7312 = rm;
    RecordSeparator = '<NewLine>';
    FieldDelimiter = '<None>';
    FieldSeparator = '<TAB>';

    schema
    {
        textelement(NodeName1)
        {
            tableelement(Integer; Integer)
            {
                AutoReplace = false;
                AutoSave = false;
                AutoUpdate = false;
                textelement(EntryNoImport)
                {
                }
                textelement(DeleteLotNoImport)
                {
                }
                textelement(DeleteSerialNoImport)
                {
                }

                trigger OnBeforeInsertRecord()
                begin
                    if not EVALUATE(EntryNo, EntryNoImport) then
                        EntryNo := 0;

                    if not EVALUATE(DeleteLotNo, DeleteLotNoImport) then
                        DeleteLotNo := false;

                    if not EVALUATE(deleteserialNo, DeleteSerialNoImport) then
                        DeleteSerialNo := false;

                    if EntryNo <> 0 then
                        UpdateWhseLedgEntry(EntryNo, DeleteLotNo, DeleteSerialNo);
                    //UpdateWhseLedgEntryDEBUG(EntryNo, DeleteLotNo, DeleteSerialNo);

                end;
            }
        }
    }

    var
        EntryNo: integer;
        WhseEntry: record "Warehouse Entry";
        DeleteSerialNo: Boolean;
        DeleteLotNo: boolean;

    procedure UpdateWhseLedgEntry(EntryNo: Integer; DeleteLotNo: Boolean; DeleteSerialNo: Boolean)
    VAR
        WriteRequired: boolean;
    begin
        if DeleteLotNo or DeleteSerialNo then begin
            with WhseEntry do begin
                if get(entryno) then begin
                    if deleteserialno and ("Serial No." <> '') then begin
                        "Serial No." := '';
                        WriteRequired := true;
                    end;
                    if deletelotno and ("Lot No." <> '') then begin
                        "Lot No." := '';
                        WriteRequired := true;
                    end;
                    if WriteRequired then
                        modify(false);
                end;
            end;
        end;
    end;

    procedure UpdateWhseLedgEntryDEBUG(EntryNo: Integer; DeleteLotNo: Boolean; DeleteSerialNo: Boolean)
    VAR
        WriteRequired: boolean;
    begin
        if DeleteLotNo or DeleteSerialNo then begin
            with WhseEntry do begin
                if get(entryno) then begin
                    if deleteserialno then "Serial No." := 'SSS';
                    if deletelotno then "Lot No." := 'LLL';
                    modify(false);
                end;
            end;
        end;
    end;

}