tableextension 50109 "RSMUSWarehouse Receipt Header" extends "Warehouse Receipt Header"
{
    fields
    {
        field(50001; RSMUSPurcOrderNo; Code[20])
        {
            Caption = 'Purchase Order No.';
            FieldClass = FlowField;
            CalcFormula = Lookup ("Warehouse Receipt Line"."Source No." WHERE("Source Document" = CONST("Purchase Order"), "No." = FIELD("No.")));
        }
        field(50002; RSMUSVendorName; Text[100])
        {
            Caption = 'Vendor Name.';

        }
    }
}