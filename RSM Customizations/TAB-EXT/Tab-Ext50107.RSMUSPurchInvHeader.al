tableextension 50107 "RSMUSPurchInvHeader" extends "Purch. Inv. Header"
{
    fields
    {
        field(50000; RSMUSShippingAgentCode; Code[10])
        {
            Caption = 'Shipping Agent Code';
            TableRelation = "Shipping Agent";
            AccessByPermission = tabledata "Shipping Agent" = r;
            DataClassification = CustomerContent;
            trigger OnValidate()
            begin
                if RSMUSShippingAgentCode <> xRec.RSMUSShippingAgentCode then
                    Validate(RSMUSShippingAgentServicecode, '');
            end;
        }
        field(50001; RSMUSShippingAgentServicecode; Code[10])
        {
            Caption = 'Shipping Agent Service Code';
            TableRelation = "Shipping Agent Services".Code where("Shipping Agent Code" = field(RSMUSShippingAgentServicecode));
            DataClassification = CustomerContent;
        }
    }
}