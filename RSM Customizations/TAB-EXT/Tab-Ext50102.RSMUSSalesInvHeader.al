/*
RSM0002 MP 05/07/2020 G007: Add new field to store Sales Inv Report
*/
tableextension 50102 "RSMUSSalesInvHeader" extends "Sales Invoice Header"
{
    fields
    {
        field(50000; "RSMUSWITS SO No."; Code[20])
        {
            CaptionML = ENU = 'WITS SO No.';
        }
        // RSM0002 >>
        field(50002; RSMUSReportAttachment; Blob)
        {
            Caption = 'Report Attachment';
        }
        // RSM0002 <<
    }
}