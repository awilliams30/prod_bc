/*
RSM0001 GHL 03/25/2020 General Revisions G004
  Added function ShowShortcutDimCode.
*/
tableextension 50106 "RSMUSPurchHeader" extends "Purchase Header" // 38
{
    fields
    {
        field(50000; RSMUSShippingAgentCode; Code[10])
        {
            Caption = 'Shipping Agent';
            TableRelation = "Shipping Agent";
            AccessByPermission = tabledata "Shipping Agent" = r;
            trigger OnValidate()
            begin
                if RSMUSShippingAgentCode <> xRec.RSMUSShippingAgentCode then
                    Validate(RSMUSShippingAgentServicecode, '');
            end;
        }
        field(50001; RSMUSShippingAgentServicecode; Code[10])
        {
            Caption = 'Shipping Agent Service';
            TableRelation = "Shipping Agent Services".Code where("Shipping Agent Code" = field(RSMUSShippingAgentCode));
        }
        field(50002; "RSMUSArrival Date"; Date)
        {
            Caption = 'Arrival Date';
        }
        field(50003; RSMUSApprovedbyUser; code[35])
        {
            Caption = 'To Be Approved by User ID';
        }
        field(50004; RSMUSStatus; code[35])
        {
            Caption = 'Status';
        }
        field(50005; RSMUSDateInitiated; DateTime)
        {
            Caption = 'Date Initiated';
        }
        field(50006; RSMUSAmountInvoiced; Decimal)
        {
            Caption = 'Amount Invoiced';
        }
        field(50007; RSMUSAmtRemainig; Decimal)
        {
            Caption = 'Invoice Amount Remaining';
        }
    }

    procedure ShowShortcutDimCode(var ShortcutDimCode: array[8] of code[20])
    var
        DimMgt: Codeunit DimensionManagement;
    begin
        DimMgt.GetShortcutDimensions("Dimension Set ID", ShortcutDimCode);
    end;
}
