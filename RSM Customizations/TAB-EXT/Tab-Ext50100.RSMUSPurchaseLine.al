/*
RSM0000 JP 12/19/19 : Using commented code to solve the requirement to filter G/L accounts based on new field
*/
tableextension 50100 "RSMUSPurchaseLine" extends "Purchase Line"
{
    fields
    {
        field(50000; "RSMUS Filtered No."; Code[20])
        {
            Caption = 'RSMUS Filted No.';
            DataClassification = CustomerContent;
            //RSM0000>>
            TableRelation = IF (Type = CONST(" ")) "Standard Text"
            ELSE
            IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(false)) "G/L Account" WHERE("Direct Posting" = CONST(true), "Account Type" = CONST(Posting), Blocked = CONST(false), RSMUSIncluded = CONST(true))
            ELSE
            IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(true)) "G/L Account" where(RSMUSIncluded = CONST(true))
            ELSE
            IF (Type = CONST("Fixed Asset")) "Fixed Asset"
            ELSE
            IF (Type = CONST("Charge (Item)")) "Item Charge"
            ELSE
            IF (Type = CONST("Item")) Item WHERE(Blocked = CONST(FALSE));
            //RSM0000<<

            //Not using process anymore
            /* TableRelation = IF (Type = CONST(" ")) "Standard Text"
             ELSE
             IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(FALSE)) "G/L Account" WHERE("Direct Posting" = CONST(TRUE), "Account Type" = CONST(Posting), Blocked = CONST(FALSE))
             ELSE
             IF (Type = CONST("G/L Account"), "System-Created Entry" = CONST(TRUE)) "G/L Account"
             ELSE
             IF (Type = CONST("Fixed Asset")) "Fixed Asset"
             ELSE
             IF (Type = CONST("Charge (Item)")) "Item Charge"
             ELSE
             IF (Type = CONST("Item"), "Document Type" = CONST(Quote)) Item WHERE(Blocked = CONST(FALSE), "No." = FILTER(<> 'S-*'))
             ELSE
             IF (Type = CONST("Item"), "Document Type" = CONST(Order)) Item WHERE(Blocked = CONST(FALSE), "No." = FILTER(<> 'S-*'))
             ELSE
             IF (Type = CONST("Item")) Item WHERE(Blocked = CONST(FALSE));*/

            ValidateTableRelation = false;
        }
        field(50003; RSMUSVariantCode; Code[10])
        {
            Caption = 'Variant Code';
            TableRelation = if (Type = filter(Item)) RSMUSItemVariants.Code where("Item No." = field("No."));
            trigger OnValidate()
            begin
                Validate("Variant Code", RSMUSVariantCode);
            end;
        }
        modify("No.")
        {
            trigger OnAfterValidate()
            begin
                "RSMUS Filtered No." := "No.";
            end;
        }




    }

}