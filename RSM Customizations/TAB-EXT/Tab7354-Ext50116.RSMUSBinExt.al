tableextension 50116 "RSMUSBinExt" extends Bin // 7354
{
    fields
    {
        field(50100; "RSMUSExclude from Sales Report"; Boolean)
        {
            Caption = 'Exclude from Sellable Inventory Reports';
        }
    }
}