tableextension 50112 "RSMUSPaymentMethod" extends "Payment Method"
{
    fields
    {
        field(50100; RSMUSWellsFargoPaymentMethod; Enum RSMUSWellsFargoPaymentMethodType)
        {
            Caption = 'Wells Fargo Payment Method';
            DataClassification = CustomerContent;
        }
    }
}