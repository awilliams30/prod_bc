/*
RSM0001 GHL 03/19/2020 Generic enhancements - BRD G003
    Added field(s) RSMUSVariant Description 2.
*/

tableextension 50118 "RSMUSItemTracingBuffer" extends "Item Tracing Buffer" // 6520
{
    fields
    {
        field(50110; "RSMUSVariant Description 2"; text[50])
        {
            Caption = 'Variant Description 2';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup ("Item Variant"."Description 2" where("Item No." = field("Item No."), Code = field("Variant Code")));
        }
    }
}