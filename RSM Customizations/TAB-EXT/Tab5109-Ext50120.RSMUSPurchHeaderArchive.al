/*
RSM0001 GHL 04/13/2020 General Revisions G004
  Added fields RSMUSAmountInvoiced, RSMUSAmtRemaining, taken from t_38 Purchase Header customizations.
  Added field RSMUSAmtRcvdNotInvoiced.
*/
tableextension 50120 "RSMUSPurchHeaderArchive" extends "Purchase Header Archive" // 5109
{
    fields
    {
        field(50006; RSMUSAmountInvoiced; Decimal)
        {
            Caption = 'Amount Invoiced';
        }
        field(50007; RSMUSAmtRemainig; Decimal)
        {
            Caption = 'Invoice Amount Remaining';
        }
        field(50100; RSMUSAmtRcvdNotInvoiced; decimal)
        {
            CalcFormula = Sum ("Purchase Line Archive"."Amt. Rcd. Not Invoiced" WHERE("Document Type" = FIELD("Document Type"),
                                                                    "Document No." = FIELD("No."),
                                                                    "Doc. No. Occurrence" = FIELD("Doc. No. Occurrence"),
                                                                    "Version No." = FIELD("Version No.")));

            Caption = 'Amount Received Not Invoiced';
            FieldClass = FlowField;
            Editable = false;
        }
    }
}
