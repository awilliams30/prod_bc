tableextension 50115 "RSMUSPositivePayDetail" extends "Positive Pay Detail"
{
    fields
    {
        field(50000; "RSMUSRecord type code2"; Code[3])
        {
            Caption = 'Record type code2';
        }
    }
}
