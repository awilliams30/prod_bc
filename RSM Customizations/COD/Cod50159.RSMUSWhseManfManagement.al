codeunit 50159 "RSMUSWhse&ManfManagement"
{
    procedure TransferShippingAgentInformation(Rec: Record "Warehouse Shipment Line")
    begin
        if Rec."Source Document" = Rec."Source Document"::"Sales Order" then begin
            WhseShipmentHeader.Reset();
            if WhseShipmentHeader.Get(Rec."No.") then begin
                SalesHeader.Reset();
                if SalesHeader.Get(SalesHeader."Document Type"::Order, Rec."Source No.") then begin
                    if (SalesHeader."Shipping Agent Code" <> '') then begin
                        WhseShipmentHeader.validate("Shipping Agent Code", SalesHeader."Shipping Agent Code");
                        WhseShipmentHeader.Modify(true);
                    end;
                    if (SalesHeader."Shipping Agent Service Code" <> '') then begin
                        WhseShipmentHeader.validate("Shipping Agent Service Code", SalesHeader."Shipping Agent Service Code");
                        WhseShipmentHeader.Modify(true);
                    end;

                    if (SalesHeader."Shipment Method Code" <> '') then begin
                        WhseShipmentHeader.Validate("Shipment Method Code", SalesHeader."Shipment Method Code");
                        WhseShipmentHeader.Modify(true);
                    end
                end;
            end;
        end;
    end;

    procedure ValidateItemTrackingCode(RecRef: RecordRef)
    var
        lDocNoFieldRef: FieldRef;
    begin
        if RecRef.Number = Database::"Warehouse Activity Line" then begin
            // clear(WhseActivityLine.Reset());
            // RecRef.SetTable(WhseActivityLine);

            lDocNoFieldRef := RecRef.Field(2);

            WhseActivityLine.Reset();
            WhseActivityLine.SetRange("Activity Type", WhseActivityLine."Activity Type"::Pick);
            WhseActivityLine.SetRange("No.", lDocNoFieldRef.Value);
            if WhseActivityLine.Find('-') then begin
                ErrorMessageList := '';
                clear(ItemList);
                repeat
                    clear(Item);
                    if Item.Get(WhseActivityLine."Item No.") then begin
                        clear(ItemTrackingCode);
                        if ItemTrackingCode.Get(Item."Item Tracking Code") then begin
                            if (WhseActivityLine."Serial No." <> '') and Not (ItemTrackingCode."SN Warehouse Tracking") then begin
                                WhseActivityLine."Serial No." := '';
                                WhseActivityLine.Modify(true);
                                if not ItemList.Contains(StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code)) then begin
                                    if ErrorMessageList = '' then
                                        ErrorMessageList += ErrorHeader;
                                    ErrorMessageList += ' \' + StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code);

                                    ItemList.add(StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code));
                                end;
                            end;

                            if (WhseActivityLine."Lot No." <> '') and Not (ItemTrackingCode."Lot Warehouse Tracking") then begin
                                WhseActivityLine."Lot No." := '';
                                WhseActivityLine.Modify(true);
                                if not ItemList.Contains(StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code)) then begin
                                    if ErrorMessageList = '' then
                                        ErrorMessageList += ErrorHeader;
                                    ErrorMessageList += ' \' + StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code);

                                    ItemList.add(StrSubstNo(InvalidItemTracking, WhseActivityLine."Item No.", ItemTrackingCode.Code));
                                end;
                            end;
                        end;
                    end;
                until WhseActivityLine.Next() = 0;
                if ItemList.Count > 0 then begin
                    Commit();
                    Error(ErrorMessageList);
                end;
            end;
        end;
    end;

    Var
        WhseShipmentHeader: Record "Warehouse Shipment Header";
        WhseActivityHeader: Record "Warehouse Activity Header";
        WhseActivityLine: Record "Warehouse Activity Line";
        SalesHeader: Record "Sales Header";
        SalesLine: Record "Sales Line";
        Item: Record Item;
        ItemTrackingCode: Record "Item Tracking Code";
        WhseShipmentLine: Record "Warehouse Shipment Line";
        ErrorHeader: TextConst ENU = 'Cannot apply Serial No. and/or Lot No. with Item Tracking Code';
        InvalidItemTracking: TextConst ENU = 'Item No. %1 : Tracking Code %2';
        SerialNoTextConst: TextConst ENU = 'serial';
        LotNoTextConst: TextConst ENU = 'lot';
        ErrorMessageList: Text;
        ItemList: List of [Text];

}