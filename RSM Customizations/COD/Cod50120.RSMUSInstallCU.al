codeunit 50120 "RSMUSInstallCU"
{
    Subtype = Install;


    trigger OnInstallAppPerCompany()
    var
        PurchaseLine: Record "Purchase Line";
        SalesLine: Record "Sales Line";
        lItemVariants: Record "Item Variant";
    begin
        PurchaseLine.Reset();
        PurchaseLine.SetFilter("RSMUS Filtered No.", '=%1', '');
        if PurchaseLine.FindSet() then begin
            repeat
                PurchaseLine."RSMUS Filtered No." := PurchaseLine."No.";
                PurchaseLine.Modify();
            until PurchaseLine.Next() <= 0;
        end;

        SalesLine.Reset();
        SalesLine.SetFilter("RSMUS Filtered No.", '=%1', '');
        if SalesLine.FindSet() then begin
            repeat
                SalesLine."RSMUS Filtered No." := SalesLine."No.";
                SalesLine.Modify();
            until SalesLine.Next() <= 0;
        end;
// 
// 
        // lItemVariants.Reset();
        // if lItemVariants.FindSet() then begin
            // repeat
                // GenMgmt.InsertOrModifyItemVariantsCustomTab(lItemVariants);
            // until lItemVariants.Next() = 0
        // end;


        // SalesLine.Reset();
        // SalesLine.SetRange("Document Type", SalesLine."Document Type"::Order);
        // if SalesLine.FindSet() then begin
            // repeat
                // if SalesLine.RSMUSVariantCode = '' then begin
                    // SalesLine.RSMUSVariantCode := SalesLine."Variant Code";
                    // SalesLine.Modify(false);
                // end;
            // until SalesLine.Next() <= 0;
        // end;
// 
        // PurchaseLine.Reset();
        // PurchaseLine.SetRange("Document Type", PurchaseLine."Document Type"::Order);
        // if PurchaseLine.FindSet() then begin
            // repeat
                // if PurchaseLine.RSMUSVariantCode = '' then begin
                    // PurchaseLine.RSMUSVariantCode := PurchaseLine."Variant Code";
                    // PurchaseLine.Modify(false);
                // end;
            // until PurchaseLine.Next() <= 0;
        // end;


    end;

    var
        GenMgmt: Codeunit RSMUSGeneralManagement;
        AllItemVariants: Record RSMUSItemVariants;
    //     ItemAttrMapping: Record "Item Attribute Value Mapping";
    //     ItemAttrMapping2: Record "Item Attribute Value Mapping";
    //     ItemAttrVal: Record "Item Attribute Value";
    //     ItemAttr: Record "Item Attribute";
}
