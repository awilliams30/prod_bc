codeunit 50150 "RSMUSGeneralEventSubscribers"
{
    [EventSubscriber(ObjectType::Table, Database::"Item Variant", 'OnAfterInsertEvent', '', false, true)]
    local procedure ItemVariantOnAfterInsertEvent(var Rec: Record "Item Variant"; RunTrigger: Boolean)
    begin
        if Rec.IsTemporary then exit;
        GenMgmt.InsertOrModifyItemVariantsCustomTab(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Variant", 'OnAfterModifyEvent', '', false, true)]
    local procedure ItemVariantOnAfterModifyEvent(var Rec: Record "Item Variant"; var xRec: Record "Item Variant"; RunTrigger: Boolean)
    begin
        if Rec.IsTemporary then exit;
        GenMgmt.InsertOrModifyItemVariantsCustomTab(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Variant", 'OnAfterDeleteEvent', '', false, true)]
    local procedure ItemVariantOnAfterDeleteEvent(var Rec: Record "Item Variant"; RunTrigger: Boolean)
    begin
        if Rec.IsTemporary then exit;
        GenMgmt.DeleteFromItemVariantsCustomTab(Rec);
        GenMgmt.DeleteFromAllItemVariantsCustomTab(Rec);
    end;

    [EventSubscriber(ObjectType::Table, Database::"Item Variant", 'OnAfterRenameEvent', '', false, true)]
    local procedure ItemVariantOnAfterRenameEvent(var Rec: Record "Item Variant"; RunTrigger: Boolean)
    begin
        if Rec.IsTemporary then exit;
        GenMgmt.InsertOrModifyItemVariantsCustomTab(Rec);
    end;


    var
        GenMgmt: Codeunit RSMUSGeneralManagement;
}