codeunit 50158 "RSMUSWhse&ManfEventSubscribers"
{
    [EventSubscriber(ObjectType::Table, database::"Warehouse Shipment Line", 'OnAfterInsertEvent', '', false, false)]
    local procedure OnAfterInsertWhseShipmentLine(var Rec: Record "Warehouse Shipment Line"; RunTrigger: Boolean)
    begin
        //if not RunTrigger then exit;
        if Rec.IsTemporary then exit;
        WhseManfMgmt.TransferShippingAgentInformation(Rec);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Whse.-Activity-Register", 'OnBeforeCode', '', false, false)]
    local procedure WhseActRegisterYesNoOnBeforeCode(var WarehouseActivityLine: Record "Warehouse Activity Line")
    var
        lRecRef: RecordRef;
    begin
        if WarehouseActivityLine.IsTemporary then exit;
        if WarehouseActivityLine."Activity Type" <> WarehouseActivityLine."Activity Type"::Pick then exit;
        lRecRef.GetTable(WarehouseActivityLine);
        WhseManfMgmt.ValidateItemTrackingCode(lRecRef);
    end;


    Var
        WhseManfMgmt: Codeunit "RSMUSWhse&ManfManagement";
}