codeunit 50118 "RSMUSOAIRecToPosPayDetail"
{
    [EventSubscriber(ObjectType::Table, 1241, 'OnAfterInsertEvent', '', false, false)]
    local procedure PosPayDetail(var Rec: Record "Positive Pay Detail";
    RunTrigger: Boolean)

    var

    begin
        if Rec."Record Type Code" = 'V' then Rec."RSMUSRecord type code2" := '370'
        else
            if Rec."Record Type Code" = 'O' then Rec."RSMUSRecord type code2" := '320';
        Rec.Modify();
    end;
}
