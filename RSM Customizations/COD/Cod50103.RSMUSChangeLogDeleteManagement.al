codeunit 50103 "RSMUSChangeLogDeleteManagement"
{
    Permissions = tabledata "Change Log Entry" = rimd, tabledata "RSMUSChange Log Delete Setup" = rimd;

    trigger OnRun()
    begin
        DeleteChangeLogEntry();
    end;

    procedure DeleteChangeLogEntry();
    var
        ChangeLogEntry: Record "Change Log Entry";
        ChangeLogDeleteSetup: Record "RSMUSChange Log Delete Setup";
        TempChangeLogEntry: Record "Change Log Entry" temporary;
        DeleteDate: Date;
        ProcessRecords: integer;
        LogEntryCount: BigInteger;
    begin
        ChangeLogDeleteSetup.Get();
        DeleteDate := CalcDate(ChangeLogDeleteSetup."Delete Date Formula", Today);
        if (DeleteDate > today) then
            exit;
        ChangeLogEntry.SetCurrentKey("Table No.", "Date and Time");
        ChangeLogEntry.SetFilter("Date and Time", '<=%1', CreateDateTime(DeleteDate, TIME));
        IF ChangeLogEntry.FindSet() then
            repeat
                ProcessRecords += 1;
                TempChangeLogEntry := ChangeLogEntry;
                TempChangeLogEntry.Insert();
            until (ChangeLogEntry.Next() = 0) or (ProcessRecords = ChangeLogDeleteSetup."Max Record Delete Count");


        TempChangeLogEntry.Reset();
        IF TempChangeLogEntry.FindSet() then
            repeat
                if ChangeLogEntry.Get(TempChangeLogEntry."Entry No.") then
                    ChangeLogEntry.Delete();
            until TempChangeLogEntry.Next() = 0;

        ChangeLogEntry.RESET;

        LogEntryCount := ChangeLogEntry.Count();
        ChangeLogDeleteSetup."Change Log Entry Count" := LogEntryCount;
        ChangeLogDeleteSetup."Last Run Delete Count" := ProcessRecords;
        ChangeLogDeleteSetup.Modify();
    end;


}