/*
RSM0001 GHL 05/29/2020 PO SO Archive Attachments BRD P002
  New object.  Trigger on insert of Sales/Purchase Archive Header, copy attachments from source order.
*/
codeunit 50105 "RSMUSDocAttachmentMgt"
{
    trigger OnRun()
    begin
    end;

    [EventSubscriber(ObjectType::Table, 5107, 'OnAfterDeleteEvent', '', false, false)]
    local procedure DeleteAttachedDocumentsOnAfterDeleteSalesHeaderArchive(var Rec: Record "Sales Header Archive"; RunTrigger: Boolean)
    var
        RecRef: RecordRef;
    begin
        RecRef.GetTable(Rec);
        DeleteAttachedDocuments(RecRef);
    end;

    [EventSubscriber(ObjectType::Table, 5107, 'OnAfterInsertEvent', '', false, false)]
    local procedure DocAttachForArchiveSalesDocs(var rec: Record "Sales Header Archive");
    var
        SalesHeader: record "Sales Header";
        FromRecRef: RecordRef;
        ToRecRef: RecordRef;
    begin
        if rec.IsTemporary then
            exit;
        if not salesheader.get(rec."Document Type", rec."No.") then
            exit;

        FromRecRef.GetTable(SalesHeader);
        ToRecRef.GetTable(rec);
        CopyAttachments(FromRecRef, ToRecRef);

    end;

    [EventSubscriber(ObjectType::Table, 5109, 'OnAfterDeleteEvent', '', false, false)]
    local procedure DeleteAttachedDocumentsOnAfterDeletePurchHeaderArchive(var Rec: Record "Purchase Header Archive"; RunTrigger: Boolean)
    var
        RecRef: RecordRef;
    begin
        RecRef.GetTable(Rec);
        DeleteAttachedDocuments(RecRef);
    end;

    [EventSubscriber(ObjectType::Table, 5109, 'OnAfterInsertEvent', '', false, false)]
    local procedure DocAttachForArchivePurchDocs(var Rec: Record "Purchase Header Archive");
    var
        PurchHeader: record "Purchase Header";
        FromRecRef: RecordRef;
        ToRecRef: RecordRef;
    begin
        if rec.IsTemporary then
            exit;
        if not purchheader.get(rec."Document Type", rec."No.") then
            exit;

        FromRecRef.GetTable(purchHeader);
        ToRecRef.GetTable(rec);
        CopyAttachments(FromRecRef, ToRecRef);

    end;

    local procedure DeleteAttachedDocuments(RecRef: RecordRef)
    var
        DocumentAttachment: Record "Document Attachment";
        FieldRef: FieldRef;
        RecNo: Code[20];
        DocType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        LineNo: Integer;
    begin
        if RecRef.IsTemporary then
            exit;
        if DocumentAttachment.IsEmpty then
            exit;
        DocumentAttachment.SetRange("Table ID", RecRef.Number);
        case RecRef.Number of
            DATABASE::Customer,
            DATABASE::Vendor,
            DATABASE::Item,
            DATABASE::Employee,
            DATABASE::"Fixed Asset",
            DATABASE::Resource,
            DATABASE::Job:
                begin
                    FieldRef := RecRef.Field(1);
                    RecNo := FieldRef.Value;
                    DocumentAttachment.SetRange("No.", RecNo);
                end;
        end;
        case RecRef.Number of
            DATABASE::"Sales Header",
            DATABASE::"Purchase Header",
            DATABASE::"Sales Line",
            DATABASE::"Purchase Line":
                begin
                    FieldRef := RecRef.Field(1);
                    DocType := FieldRef.Value;
                    DocumentAttachment.SetRange("Document Type", DocType);

                    FieldRef := RecRef.Field(3);
                    RecNo := FieldRef.Value;
                    DocumentAttachment.SetRange("No.", RecNo);
                end;
        end;
        case RecRef.Number of
            DATABASE::"Sales Line",
            DATABASE::"Purchase Line":
                begin
                    FieldRef := RecRef.Field(4);
                    LineNo := FieldRef.Value;
                    DocumentAttachment.SetRange("Line No.", LineNo);
                end;
        end;
        case RecRef.Number of
            DATABASE::"Sales Invoice Header",
            DATABASE::"Sales Invoice Line",
            DATABASE::"Sales Cr.Memo Header",
            DATABASE::"Sales Cr.Memo Line",
            DATABASE::"Purch. Inv. Header",
            DATABASE::"Purch. Inv. Line",
            DATABASE::"Purch. Cr. Memo Hdr.",
            DATABASE::"Purch. Cr. Memo Line":
                begin
                    FieldRef := RecRef.Field(3);
                    RecNo := FieldRef.Value;
                    DocumentAttachment.SetRange("No.", RecNo);
                end;
        end;
        DocumentAttachment.DeleteAll;
    end;

    procedure IsDuplicateFile(TableID: Integer; DocumentNo: Code[20]; RecDocType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order"; RecLineNo: Integer; FileName: Text; FileExtension: Text): Boolean
    var
        DocumentAttachment: Record "Document Attachment";
    begin
        DocumentAttachment.SetRange("Table ID", TableID);
        DocumentAttachment.SetRange("No.", DocumentNo);
        DocumentAttachment.SetRange("Document Type", RecDocType);
        DocumentAttachment.SetRange("Line No.", RecLineNo);
        DocumentAttachment.SetRange("File Name", FileName);
        DocumentAttachment.SetRange("File Extension", FileExtension);

        if not DocumentAttachment.IsEmpty then
            exit(true);

        exit(false);
    end;

    local procedure CopyAttachments(var FromRecRef: RecordRef; var ToRecRef: RecordRef)
    var
        FromDocumentAttachment: Record "Document Attachment";
        ToDocumentAttachment: Record "Document Attachment";
        FromFieldRef: FieldRef;
        ToFieldRef: FieldRef;
        FromDocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        FromLineNo: Integer;
        FromNo: Code[20];
        ToNo: Code[20];
        ToDocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        ToLineNo: Integer;
    begin
        FromDocumentAttachment.SetRange("Table ID", FromRecRef.Number);
        if FromDocumentAttachment.IsEmpty then
            exit;

        case FromRecRef.Number of
            DATABASE::Customer,
            DATABASE::Vendor,
            DATABASE::Item:
                begin
                    FromFieldRef := FromRecRef.Field(1);
                    FromNo := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("No.", FromNo);
                end;
            DATABASE::"Sales Header",
            DATABASE::"Purchase Header":
                begin
                    FromFieldRef := FromRecRef.Field(1);
                    FromDocumentType := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("Document Type", FromDocumentType);
                    FromFieldRef := FromRecRef.Field(3);
                    FromNo := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("No.", FromNo);
                end;
            DATABASE::"Sales Line",
            DATABASE::"Purchase Line":
                begin
                    FromFieldRef := FromRecRef.Field(1);
                    FromDocumentType := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("Document Type", FromDocumentType);
                    FromFieldRef := FromRecRef.Field(3);
                    FromNo := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("No.", FromNo);
                    FromFieldRef := FromRecRef.Field(4);
                    FromLineNo := FromFieldRef.Value;
                    FromDocumentAttachment.SetRange("Line No.", FromLineNo);
                end
        end;

        if FromDocumentAttachment.FindSet then begin

            case ToRecRef.Number of
                DATABASE::"Sales Header Archive",
                DATABASE::"Purchase Header Archive":
                    begin
                        ToFieldRef := ToRecRef.Field(1);
                        ToDocumentType := ToFieldRef.Value;
                    end;
            end;

            ToFieldRef := ToRecRef.Field(3); // Document No.
            ToNo := ToFieldRef.Value;
            ToFieldRef := ToRecRef.Field(5047); // Version No.
            ToLineNo := ToFieldRef.Value;

            repeat
                Clear(ToDocumentAttachment);
                ToDocumentAttachment.Init;
                ToDocumentAttachment.TransferFields(FromDocumentAttachment);
                ToDocumentAttachment.Validate("Table ID", ToRecRef.Number);
                ToDocumentAttachment.Validate("No.", ToNo);
                ToDocumentAttachment.Validate("Document Type", ToDocumentType);
                ToDocumentAttachment.Validate("Line No.", ToLineNo);
                ToDocumentAttachment.id := 0;

                if not ToDocumentAttachment.Insert(true) then;

            until FromDocumentAttachment.Next = 0;
        end;

        // Copies attachments for header and then calls CopyAttachmentsForPostedDocsLines to copy attachments for lines.
    end;

    local procedure CopyAttachmentsForPostedDocs(var FromRecRef: RecordRef; var ToRecRef: RecordRef)
    var
        FromDocumentAttachment: Record "Document Attachment";
        ToDocumentAttachment: Record "Document Attachment";
        FromFieldRef: FieldRef;
        ToFieldRef: FieldRef;
        FromDocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        FromNo: Code[20];
        ToNo: Code[20];
    begin
        FromDocumentAttachment.SetRange("Table ID", FromRecRef.Number);

        FromFieldRef := FromRecRef.Field(1);
        FromDocumentType := FromFieldRef.Value;
        FromDocumentAttachment.SetRange("Document Type", FromDocumentType);

        FromFieldRef := FromRecRef.Field(3);
        FromNo := FromFieldRef.Value;
        FromDocumentAttachment.SetRange("No.", FromNo);

        // Find any attached docs for headers (sales / purch)
        if FromDocumentAttachment.FindSet then begin
            repeat
                Clear(ToDocumentAttachment);
                ToDocumentAttachment.Init;
                ToDocumentAttachment.TransferFields(FromDocumentAttachment);
                ToDocumentAttachment.Validate("Table ID", ToRecRef.Number);

                ToFieldRef := ToRecRef.Field(3);
                ToNo := ToFieldRef.Value;
                ToDocumentAttachment.Validate("No.", ToNo);
                Clear(ToDocumentAttachment."Document Type");
                ToDocumentAttachment.Insert(true);

            until FromDocumentAttachment.Next = 0;
        end;
        CopyAttachmentsForPostedDocsLines(FromRecRef, ToRecRef);
    end;

    local procedure CopyAttachmentsForPostedDocsLines(var FromRecRef: RecordRef; var ToRecRef: RecordRef)
    var
        FromDocumentAttachmentLines: Record "Document Attachment";
        ToDocumentAttachmentLines: Record "Document Attachment";
        FromFieldRef: FieldRef;
        ToFieldRef: FieldRef;
        FromDocumentType: Option Quote,"Order",Invoice,"Credit Memo","Blanket Order","Return Order";
        FromNo: Code[20];
        ToNo: Code[20];
    begin
        FromFieldRef := FromRecRef.Field(3);
        FromNo := FromFieldRef.Value;
        FromDocumentAttachmentLines.Reset;

        FromFieldRef := FromRecRef.Field(1);
        FromDocumentType := FromFieldRef.Value;
        FromDocumentAttachmentLines.SetRange("Document Type", FromDocumentType);

        ToFieldRef := ToRecRef.Field(3);
        ToNo := ToFieldRef.Value;

        case FromRecRef.Number of
            DATABASE::"Sales Header":
                FromDocumentAttachmentLines.SetRange("Table ID", DATABASE::"Sales Line");
            DATABASE::"Purchase Header":
                FromDocumentAttachmentLines.SetRange("Table ID", DATABASE::"Purchase Line");
        end;
        FromDocumentAttachmentLines.SetRange("No.", FromNo);
        FromDocumentAttachmentLines.SetRange("Document Type", FromDocumentType);

        if FromDocumentAttachmentLines.FindSet then
            repeat
                ToDocumentAttachmentLines.TransferFields(FromDocumentAttachmentLines);
                case ToRecRef.Number of
                    DATABASE::"Sales Invoice Header":
                        ToDocumentAttachmentLines.Validate("Table ID", DATABASE::"Sales Invoice Line");
                    DATABASE::"Sales Cr.Memo Header":
                        ToDocumentAttachmentLines.Validate("Table ID", DATABASE::"Sales Cr.Memo Line");
                    DATABASE::"Purch. Inv. Header":
                        ToDocumentAttachmentLines.Validate("Table ID", DATABASE::"Purch. Inv. Line");
                    DATABASE::"Purch. Cr. Memo Hdr.":
                        ToDocumentAttachmentLines.Validate("Table ID", DATABASE::"Purch. Cr. Memo Line");
                end;

                Clear(ToDocumentAttachmentLines."Document Type");
                ToDocumentAttachmentLines.Validate("No.", ToNo);

                if ToDocumentAttachmentLines.Insert(true) then;
            until FromDocumentAttachmentLines.Next = 0;
    end;

    local procedure MoveAttachmentsWithinSameRecordType(var MoveFromRecRef: RecordRef; var MoveToRecRef: RecordRef)
    var
        DocumentAttachmentFound: Record "Document Attachment";
        DocumentAttachmentToCreate: Record "Document Attachment";
        MoveFromFieldRef: FieldRef;
        MoveToFieldRef: FieldRef;
        MoveFromRecNo: Code[20];
        MoveToRecNo: Code[20];
    begin
        // Moves attachments from one record to another for same type
        if MoveFromRecRef.Number <> MoveToRecRef.Number then
            exit;
        if MoveFromRecRef.IsTemporary or MoveToRecRef.IsTemporary then
            exit;

        DocumentAttachmentFound.SetRange("Table ID", MoveFromRecRef.Number);
        case MoveFromRecRef.Number of
            DATABASE::Customer,
            DATABASE::Vendor,
            DATABASE::Job,
            DATABASE::Employee,
            DATABASE::"Fixed Asset",
            DATABASE::Resource,
            DATABASE::Item:
                begin
                    MoveFromFieldRef := MoveFromRecRef.Field(1);
                    MoveFromRecNo := MoveFromFieldRef.Value;
                    MoveToFieldRef := MoveToRecRef.Field(1);
                    MoveToRecNo := MoveToFieldRef.Value;
                    DocumentAttachmentFound.SetRange("No.", MoveFromRecNo);
                end;
        end;

        // Find any attached docs to be moved
        if DocumentAttachmentFound.IsEmpty then
            exit;

        // Create a copy of all found attachments with new number [MoveToRecNo]
        // Need to do this because MODIFY does not support renaming keys for a record.
        if DocumentAttachmentFound.FindSet then begin
            repeat
                Clear(DocumentAttachmentToCreate);
                DocumentAttachmentToCreate.Init;
                DocumentAttachmentToCreate.TransferFields(DocumentAttachmentFound);
                DocumentAttachmentToCreate.Validate("No.", MoveToRecNo);
                DocumentAttachmentToCreate.Insert(true);
            until DocumentAttachmentFound.Next = 0;
        end;

        // Delete orphan attachments
        DocumentAttachmentFound.DeleteAll(true);
    end;
}

