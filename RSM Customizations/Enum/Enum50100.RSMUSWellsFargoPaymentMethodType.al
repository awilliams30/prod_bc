enum 50100 "RSMUSWellsFargoPaymentMethodType"
{
    Extensible = true;
    AssignmentCompatibility = true;

    value(0; " ") { Caption = ' '; }
    value(1; "CCR") { Caption = 'CCR'; }
    value(2; "CHK") { Caption = 'CHK'; }
    value(3; "DAC") { Caption = 'DAC'; }
    value(4; "IWI") { Caption = 'IWI'; }
    value(5; "MTS") { Caption = 'MTS'; }
}