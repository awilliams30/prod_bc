query 50102 "RSMUSSalesLineRemainQty" // 50102
{
    QueryType = Normal;
    Caption = 'Sales Line Remaining Quantity';
    Permissions = tabledata "Sales Line" = r;
    elements
    {
        dataitem(SalesLine; "Sales Line")
        {
            DataItemTableFilter = "Document Type" = filter(Order), Type = filter(Item), "Outstanding Quantity" = filter('>0');

            column(ItemNo; "No.")
            {
            }
            column(VariantCode; "Variant Code")
            {

            }
            column(LocationCode; "Location Code")
            {
            }
            column(UnitofMeasure; "Unit of Measure")
            {
            }
            column(RemainingQuantity; "Outstanding Quantity")
            {
            }
            column(RemainingQuantityBase; "Outstanding Qty. (Base)")
            {

            }
            dataitem(Item; Item)
            {
                DataItemLink = "No." = salesline."No.";
                column(ItemType; Type)
                {

                }

            }

        }
    }
}