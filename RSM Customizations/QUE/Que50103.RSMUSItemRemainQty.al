query 50103 "RSMUSItemRemainQty" // 50103
{
    Caption = 'Item Remaining Quantity';
    QueryType = Normal;
    Permissions = tabledata "Item Ledger Entry" = r, tabledata Item = r;

    elements
    {
        dataitem(ItemLedgEntry; "Item Ledger Entry")
        {

            DataItemTableFilter = open = filter(TRUE);
            column(ItemNo; "Item No.")
            {
            }
            column(VariantCode; "Variant Code")
            {
            }
            column(LocationCode; "Location Code")
            { }
            column(UnitofMeasureCode; "Unit of Measure Code")
            {
            }
            column(QtyperUnitofMeasure; "Qty. per Unit of Measure")
            {
            }
            column(Quantity; Quantity)
            {
            }
            column(RemainingQuantity; "Remaining Quantity")
            {
            }

            dataitem(Item; Item)
            {
                DataItemLink = "No." = ItemLedgEntry."Item No.";
                column(ItemType; Type)
                {

                }

            }
        }
    }
}