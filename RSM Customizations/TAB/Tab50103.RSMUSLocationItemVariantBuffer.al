
table 50103 "RSMUSLocationItemVariantBuffer" // 50103
{
    Caption = 'Location Item Variant Buffer';
    fields
    {
        field(1; "Item No."; Code[20])
        {
            TableRelation = Item."No.";
        }
        field(2; "Variant Code"; Code[10])
        {
            TableRelation = "Item Variant".Code where("Item No." = field("Item No."));
        }
        field(3; "Location Code"; Code[10])
        {
            TableRelation = Location.Code;
        }
        field(4; "Label"; Text[250])
        {
        }
        field(5; "Remaining Quantity"; Decimal)
        {
        }
        field(6; Value1; Decimal)
        {
        }
        field(7; Value2; Decimal)
        {
        }
        field(8; Value3; Decimal)
        {
        }
        field(9; Value4; Decimal)
        {
        }
        field(10; Value5; Decimal)
        {
        }
        field(11; Value6; Decimal)
        {
        }
        field(12; Value7; Decimal)
        {
        }
        field(13; Value8; Decimal)
        {
        }
        field(14; Value9; Decimal)
        {
        }
        field(15; Value10; Decimal)
        {
        }
        field(16; Value11; Decimal)
        {
        }
        field(50100; "Item Description"; text[100])
        {
            Caption = 'Item Description';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup (item.Description where("No." = field("Item No.")));
        }
        field(50110; "Variant Description 2"; text[100])
        {
            Caption = 'Variant Description 2';
            FieldClass = FlowField;
            Editable = false;
            CalcFormula = lookup ("Item Variant"."Description 2" where("Item No." = field("Item No."), Code = field("Variant Code")));
        }

    }
    keys
    {
        key(PK; "Location Code", "Item No.", "Variant Code")
        {
            Clustered = true;
        }
    }
}