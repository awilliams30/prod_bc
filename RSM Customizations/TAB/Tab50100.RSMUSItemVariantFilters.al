table 50100 "RSMUSItemVariantFilters"
{
    DrillDownPageId = RSMUSAllItemVariants;
    //LookupPageId = "Item Variants";
    Permissions = tabledata "RSMUSAllItemVariants" = rmid, tabledata "RSMUSItemVariantFilters" = rmid;
    ;

    fields
    {
        field(50100; PrimaryKey; Code[10])
        {
            Caption = 'Primary Key';
        }
        field(50101; RSMUSItemFilter; Code[20])
        //field(50101; RSMUSItemFilter; Text[100])
        {
            Caption = 'Item Filter';
            //TableRelation = Item."No.";
            //TestTableRelation = false;
            //ValidateTableRelation = false;
        }
        field(50102; RSMUSVariantFilter; Code[10])
        {
            Caption = 'Variant Filter';
        }
        field(50103; RSMUSVariantDescFilter; Text[100])
        {
            Caption = 'Variant Desc. Filter';
        }
        field(50104; RSMUSVariantDesc2Filter; text[50])
        {
            Caption = 'Variant Desc. 2 Filter';
        }
    }

    keys
    {
        key(PK; PrimaryKey)
        {
            Clustered = true;
        }
    }
}