table 50102 "RSMUSChange Log Delete Setup"
{
    DataClassification = ToBeClassified;

    fields
    {
        field(1; "Primary Key"; Code[10])
        {
            DataClassification = ToBeClassified;
        }
        field(2; "Max Record Delete Count"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(3; "Delete Date Formula"; DateFormula)
        {
            DataClassification = ToBeClassified;
        }
        field(4; "Change Log Entry Count"; Integer)
        {
            DataClassification = ToBeClassified;
        }
        field(5; "Last Run Delete Count"; Integer)
        {
            DataClassification = ToBeClassified;
        }

    }

    keys
    {
        key(PK; "Primary Key")
        {
            Clustered = true;
        }
    }

}