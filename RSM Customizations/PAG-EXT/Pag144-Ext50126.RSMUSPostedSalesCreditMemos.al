pageextension 50126 "RSMUSPostedSalesCreditMemos" extends "Posted Sales Credit Memos" //144
{
    layout
    {
        addafter("Sell-to Customer Name")
        {
            field("RSMUSExternal Document No."; "External Document No.")
            {
                ApplicationArea = all;
            }
        }
    }
}