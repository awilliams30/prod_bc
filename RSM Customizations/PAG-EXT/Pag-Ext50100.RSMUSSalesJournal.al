pageextension 50100 "RSMUSSalesJournal" extends "Sales Journal"
{
    layout
    {

        addafter("Currency Code")
        {
            field("Posting Group"; "Posting Group")
            {
                ApplicationArea = All;
                Editable = true;
            }
        }

        modify("Account Type")
        {
            ApplicationArea = All;
        }

    }
}

