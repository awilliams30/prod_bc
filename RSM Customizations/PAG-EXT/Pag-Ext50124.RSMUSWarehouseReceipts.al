pageextension 50124 "RSMUSWarehouse Receipts" extends "Warehouse Receipts"
{
    layout
    {
        addafter("No.")
        {
            field(RSMUSVendorName; RSMUSVendorName)
            {
                ApplicationArea = all;
            }
            field(RSMUSPurcOrderNo; RSMUSPurcOrderNo)
            {
                ApplicationArea = All;
            }
            field("RSMUSVendor Shipment No."; "Vendor Shipment No.")
            {
                ApplicationArea = All;
            }
            field("RSMUSPosting Date"; "Posting Date")
            {
                Caption = 'Post Date of Warehouse Receipt No.';
                ApplicationArea = all;
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    //RSM0000>>
    trigger OnAfterGetRecord()
    var
        Vendor: Record vendor;
        WheLine: Record "Warehouse Receipt Line";
        Purchase: Record "Purchase Header";
    begin
        WheLine.SetFilter(WheLine."Source Document", '=%1', WheLine."Source Document"::"Purchase Order");
        WheLine.SetFilter(WheLine."No.", '=%1', Rec."No.");
        if WheLine.FindFirst() then begin
            Purchase.SetFilter(Purchase."No.", '=%1', WheLine."Source No.");
            if Purchase.FindFirst() then begin
                Vendor.SetFilter(Vendor."No.", '=%1', Purchase."Buy-from Vendor No.");
                if Vendor.FindFirst() then
                    RSMUSVendorName := Vendor.Name;
            end;
        end;
    end;
    //RSM0000<<
    var
        myInt: Integer;
        WarehouseLines: Record "Warehouse Receipt Line";
}