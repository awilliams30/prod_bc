
Pageextension 50108 "RSMUSSalesInvoiceSubForm" extends "Sales Invoice Subform"
{
    layout
    {
        addbefore(Description)
        {
            /* //Not using process anymore
            field("RSMUS Filtered No."; "RSMUS Filtered No.")
            {
                ApplicationArea = All;
                Caption = 'No.';
                trigger OnValidate()
                begin
                    if CopyStr("RSMUS Filtered No.", 1, 2) <> 'P-' then begin
                        Rec.Validate("No.", "RSMUS Filtered No.");
                        Rec."RSMUS Filtered No." := Rec."No.";
                        CurrPage.Update();
                    end else
                        Error('The Item entered is not a valid Sales Item');
                end;
            }*/
            field("RSMUS Customer Ref No."; "RSMUS Customer Ref No.")
            {
                ApplicationArea = All;
            }
        }
        /*
        addbefore("RSMUS Filtered No.")
        {
            field("RSMUS Facebook Line No."; "RSMUS Facebook Line No.")
            {
                ApplicationArea = All;
            }
        }
        */
        /*
        modify("No.")
        {
            Visible = false;
        }
        */
    }
}
