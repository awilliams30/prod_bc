/*

RSM0002 MP 09/09/2020 : Update filtered no page validation to also error if manually entering gl not found with same filters as table relation

*/
pageextension 50111 "RSMUSPurchaseOrderSubform" extends "Purchase Order Subform"
{
    layout
    {
        addbefore(Description)
        {
            //Not using process anymore
            /* field("RSMUS Filtered No."; "RSMUS Filtered No.")
             {
                 ApplicationArea = All;
                 Caption = 'No.';
                 trigger OnValidate()
                 begin
                     if CopyStr("RSMUS Filtered No.", 1, 2) <> 'S-' then begin
                         Rec.Validate("No.", "RSMUS Filtered No.");
                         Rec."RSMUS Filtered No." := Rec."No.";
                         CurrPage.Update();
                     end else
                         Error('The Item entered is not a valid Purchase Item');
                 end;
             }*/
            field("RSMUS Filtered No."; "RSMUS Filtered No.")
            {
                ApplicationArea = All;
                Caption = 'No.';
                trigger OnValidate()
                var
                    GLAccounts: Record "G/L Account";
                begin
                    Rec.Validate("No.", "RSMUS Filtered No.");
                    Rec."RSMUS Filtered No." := Rec."No.";
                    //RSM0002>>
                    //CurrPage.Update();
                    if (Type = Type::"G/L Account") and Not ("System-Created Entry") then begin
                        Clear(GLAccounts);
                        GLAccounts.Reset();
                        GLAccounts.SetRange("Direct Posting", true);
                        GLAccounts.SetRange("Account Type", GLAccounts."Account Type"::Posting);
                        GLAccounts.SetRange(Blocked, false);
                        GLAccounts.SetRange(RSMUSIncluded, true);
                        GLAccounts.SetRange("No.", "No.");
                        if Not (GLAccounts.FindFirst()) then
                            Error(FilteredItemNoError, "No.");
                    end else begin
                        if (Type = Type::"G/L Account") and "System-Created Entry" then begin
                            Clear(GLAccounts);
                            GLAccounts.Reset();
                            GLAccounts.SetRange(RSMUSIncluded, true);
                            GLAccounts.SetRange("No.", "No.");
                            if Not (GLAccounts.FindFirst()) then
                                Error(FilteredItemNoError, "No.");
                        end;
                    end;

                    CurrPage.Update();
                    //RSM0002<<

                end;
            }
        }
        addafter("Location Code")
        {
            field("RSMUSVariant Code"; RSMUSVariantCode)
            {
                ApplicationArea = all;
                Visible = true;
            }
        }
        modify("Variant Code")
        {
            Visible = false;
            ApplicationArea = none;
        }
        modify("No.")
        {
            Visible = false;
            Editable = false;
            ApplicationArea = none;
        }

    }
    var
        FilteredItemNoError: Label 'The G/L Account does not exist as a valid selection. Identification fields and values: No.=%1';
}


