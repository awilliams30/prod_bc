pageextension 50106 "RSMUSSalesOrder" extends "Sales Order"
{
    layout
    {
        modify("Document Date")
        {
            Caption = 'Sales Order Date';
        }
        modify("External Document No.")
        {
            Caption = 'Customer PO No.';
        }
        addafter("Posting Date")
        {
            // field("RSMUSOrder Type"; "RSMUSOrder Type")
            // {
            //     Caption = 'Order Type';
            //     ApplicationArea = all;
            // }
            field("Client Code"; ShortcutDimCode[2])
            {
                Caption = 'Client Code';
                ApplicationArea = Dimensions;
                //CaptionClass = '1,2,3';
                TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(2),
                                                                  "Dimension Value Type" = CONST(Standard),
                                                                  "Blocked" = CONST(false));
                trigger OnValidate()
                begin
                    DimMgt.ValidateShortcutDimValues(2, ShortcutDimCode[2], Rec."Dimension Set ID");
                end;
            }
            field(Facility; ShortcutDimCode[3])
            {
                Caption = 'Facility';
                ApplicationArea = Dimensions;
                //CaptionClass = '1,2,4';
                TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(3),
                                                                  "Dimension Value Type" = CONST(Standard),
                                                                  "Blocked" = CONST(false));
                trigger OnValidate()
                begin
                    DimMgt.ValidateShortcutDimValues(3, ShortcutDimCode[3], Rec."Dimension Set ID");
                end;
            }
            field("Order Type"; ShortcutDimCode[6])
            {
                Caption = 'Order Type';
                ApplicationArea = Dimensions;
                //CaptionClass = '1,2,3';
                TableRelation = "Dimension Value".Code WHERE("Global Dimension No." = CONST(6),
                                                                  "Dimension Value Type" = CONST(Standard),
                                                                  "Blocked" = CONST(false));
                trigger OnValidate()
                begin
                    DimMgt.ValidateShortcutDimValues(6, ShortcutDimCode[6], Rec."Dimension Set ID");
                end;
            }
        }
        addafter("Outbound Whse. Handling Time")
        {
            field("RSMUSPackage Tracking No."; "Package Tracking No.")
            {
                ApplicationArea = all;
            }
        }
    }
    trigger OnAfterGetRecord()
    begin
        DimMgt.GetShortcutDimensions(Rec."Dimension Set ID", ShortcutDimCode);
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        CLEAR(ShortcutDimCode);
    end;

    var
        ShortcutDimCode: array[8] of Code[20];
        DimMgt: Codeunit DimensionManagement;
}
