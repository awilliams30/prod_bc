/*
RSM0001 GHL 03/27/2020 General Enhancements G004
  Added field "RSMUS Filtered No."
  Modified properties for standard "No." field
    Enabled => false, Visible => false.

RSM0002 MP 09/09/2020 : Update filtered no page validation to also error if manually entering gl not found with same filters as table relation
*/

pageextension 50109 "RSMUSSalesOrderSubform" extends "Sales Order Subform" // 46
{
    layout
    {
        addbefore(Description)
        {
            field("RSMUS Filtered No."; "RSMUS Filtered No.")
            {
                ApplicationArea = All;
                Caption = 'No.';
                trigger OnValidate()
                var
                    GLAccounts: Record "G/L Account";
                begin
                    Rec.Validate("No.", "RSMUS Filtered No.");
                    Rec."RSMUS Filtered No." := Rec."No.";
                    //RSM0002 >>
                    //CurrPage.Update();
                    if (Type = Type::"G/L Account") and Not ("System-Created Entry") then begin
                        Clear(GLAccounts);
                        GLAccounts.Reset();
                        GLAccounts.SetRange("Direct Posting", true);
                        GLAccounts.SetRange("Account Type", GLAccounts."Account Type"::Posting);
                        GLAccounts.SetRange(Blocked, false);
                        GLAccounts.SetRange(RSMUSIncludeOnSO, true);
                        GLAccounts.SetRange("No.", "No.");
                        if Not (GLAccounts.FindFirst()) then
                            Error(FilteredItemNoError, "No.");
                    end else begin
                        if (Type = Type::"G/L Account") and "System-Created Entry" then begin
                            Clear(GLAccounts);
                            GLAccounts.Reset();
                            GLAccounts.SetRange(RSMUSIncludeOnSO, true);
                            GLAccounts.SetRange("No.", "No.");
                            if Not (GLAccounts.FindFirst()) then
                                Error(FilteredItemNoError, "No.");
                        end;
                    end;
                    CurrPage.Update();
                    //RSM0002<<
                end;
            }
        }
        modify("No.")
        {
            Visible = false;
            Enabled = false;
            Applicationarea = none;
        }
        addafter("Location Code")
        {
            field("RSMUSVariant Code"; RSMUSVariantCode)
            {
                ApplicationArea = all;
                Visible = true;
            }
        }
        modify("Location Code")
        {
            Visible = true;
        }
        modify("Variant Code")
        {
            Visible = false;
            ApplicationArea = none;
        }
    }
    var
        FilteredItemNoError: Label 'The G/L Account does not exist as a valid selection. Identification fields and values: No.=%1';
}



