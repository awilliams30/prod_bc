pageextension 50120 "RSMUSProdOrderComponents" extends "Prod. Order Components"
{

    actions
    {
        addafter("&Print")
        {
            action(RSMUSPrint2)
            {
                ApplicationArea = All;
                Caption = 'Print';
                Image = Print;

                trigger OnAction()
                var
                    lProdOrderComp: Record "Prod. Order Component";
                begin
                    lProdOrderComp.Copy(Rec);
                    Report.RunModal(Report::RSMUSProdOrderPickingList, true, true, lProdOrderComp);
                end;
            }
        }
        modify("&Print")
        {
            ApplicationArea = none;
            // trigger OnAfterAction()
            // var
            //     lProdOrderComp: Record "Prod. Order Component";
            // begin
            //     lProdOrderComp.Copy(Rec);
            //     Report.RunModal(Report::RSMUSProdOrderPickingList, true, true, lProdOrderComp);
            // end;
        }
    }
}