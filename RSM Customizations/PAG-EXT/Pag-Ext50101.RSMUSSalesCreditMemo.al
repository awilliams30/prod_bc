pageextension 50101 "RSMUSSalesCreditMemo" extends "Sales Credit Memo"
{
    layout
    {
        addafter("Applies-to ID")
        {
            field("Customer Posting Group"; "Customer Posting Group")
            {
                ApplicationArea = All;
                Editable = true;
            }
        }
    }
}