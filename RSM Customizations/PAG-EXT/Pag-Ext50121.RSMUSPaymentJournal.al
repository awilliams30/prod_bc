pageextension 50121 "RSMUSPaymentJournal" extends "Payment Journal"
{

    actions
    {
        addafter("P&osting")
        {
            action(RSMUSOverrideApprovalEntries)
            {
                Caption = 'Override Active Workflow';
                ApplicationArea = all;
                AccessByPermission = codeunit "RSMUSOverrideApprovalEntries" = x;
                trigger OnAction()
                var
                    lOverrideActiveWorkflows: Codeunit RSMUSOverrideApprovalEntries;
                begin
                    lOverrideActiveWorkflows.ImportApprovalEntries();
                end;
            }
        }
    }
}