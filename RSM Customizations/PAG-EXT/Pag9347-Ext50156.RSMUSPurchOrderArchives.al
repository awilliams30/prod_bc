/*
RSM0001 GHL 04/10/2020 Generic Enhancements BRD G004
  New page extension.
RSM0002 GHL 05/29/2020 PO SO Archive Attachments BRD P002
  Added Attachments menu option.
*/
pageextension 50156 "RSMUSPurchOrderArchives" extends "Purchase Order Archives" // 9347
{
    layout
    {
        addlast(Control1)
        {
            field("RSMUSVendor Shipment No."; "Vendor Shipment No.")
            {
                ApplicationArea = All;
            }
            field("RSMUSVendor Order No."; "Vendor Order No.")
            {
                ApplicationArea = All;
            }
            field(RSMUSAmtRcvdNotInvoiced; RSMUSAmtRcvdNotInvoiced)
            {
                ApplicationArea = All;
            }
            field(RSMUSAmount; Amount)
            {
                ApplicationArea = All;
            }
            field(RSMUSAmountInvoiced; RSMUSAmountInvoiced)
            {
                ApplicationArea = All;
            }
            field(RSMUSAmtRemainig; RSMUSAmtRemainig)
            {
                ApplicationArea = All;
            }
            field("RSMUSVendor Invoice No."; "Vendor Invoice No.")
            {
                ApplicationArea = All;
            }
            field("RSMUSPosting Description"; "Posting Description")
            {
                ApplicationArea = All;
            }
        }
    }
    actions
    {
        addlast(navigation)
        {
            action(RSMUSShowAttachments)
            {
                Caption = 'Attachments';
                ApplicationArea = All;
                trigger OnAction()
                var
                    DocAttachment: record "Document Attachment";
                    DocAttachmentList: page 1173;
                begin
                    with DocAttachment do begin
                        setfilter("Table ID", '%1', database::"Purchase Header Archive");
                        //setfilter("Table ID", '%1', 5107);
                        setfilter("Document Type", '%1', rec."Document Type");
                        SetFilter("No.", '%1', rec."No.");
                        SetFilter("Line No.", '%1', rec."Version No.");
                    end;
                    with DocAttachmentList do begin
                        Editable := false;
                        SetTableView(DocAttachment);
                        runmodal;
                    end;
                end;
            }
        }

    }

}