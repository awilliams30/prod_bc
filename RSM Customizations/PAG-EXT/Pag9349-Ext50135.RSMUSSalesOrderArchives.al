/*
RSM0001 GHL 05/29/2020 PO SO Archive Attachments BRD P002
  Added Attachments menu option.
*/

pageextension 50135 "RSMUSSalesOrderArchives" extends "Sales Order Archives" // 9349
{
    actions
    {
        addlast(navigation)
        {
            action(RSMUSShowAttachments)
            {
                Caption = 'Attachments';
                ApplicationArea = All;
                trigger OnAction()
                var
                    DocAttachment: record "Document Attachment";
                    DocAttachmentList: page 1173;
                begin
                    with DocAttachment do begin
                        setfilter("Table ID", '%1', database::"Sales Header Archive");
                        //setfilter("Table ID", '%1', 5107);
                        setfilter("Document Type", '%1', rec."Document Type");
                        SetFilter("No.", '%1', rec."No.");
                        SetFilter("Line No.", '%1', rec."Version No.");
                    end;
                    with DocAttachmentList do begin
                        Editable := false;
                        SetTableView(DocAttachment);
                        runmodal;

                    end;
                end;
            }
        }

    }
}