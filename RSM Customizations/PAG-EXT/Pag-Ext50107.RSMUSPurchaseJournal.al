pageextension 50107 "RSMUSPurchaseJournal" extends "Purchase Journal"
{
    actions
    {
        addafter("P&osting")
        {
            group("RSMUSRequest Approval")
            {
                Caption = 'Request Approval';
                group("RSMUSSendApprovalRequest")
                {
                    Caption = 'Send Approval Request';
                    Image = SendApprovalRequest;
                    action("RSMUSSendApprovalRequestJournalBatch")
                    {
                        //ApplicationArea = Basic, Suite;
                        ApplicationArea = All;
                        Caption = 'Journal Batch';
                        //Enabled = NOT OpenApprovalEntriesOnBatchOrAnyJnlLineExist AND CanRequestFlowApprovalForBatchAndAllLines;
                        Image = SendApprovalRequest;
                        ToolTip = 'Send all journal lines for approval, also those that you may not see because of filters.';

                        trigger OnAction()
                        var
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                        begin
                            ApprovalsMgmt.TrySendJournalBatchApprovalRequest(Rec);
                            SetControlAppearanceFromBatch;
                            SetControlAppearance;
                        end;
                    }
                    action("RSMUSSendApprovalRequestJournalLine")
                    {
                        //ApplicationArea = Basic, Suite;
                        ApplicationArea = All;
                        Caption = 'Selected Journal Lines';
                        //Enabled = NOT OpenApprovalEntriesOnBatchOrCurrJnlLineExist AND CanRequestFlowApprovalForBatchAndCurrentLine;
                        Image = SendApprovalRequest;
                        ToolTip = 'Send selected journal lines for approval.';

                        trigger OnAction()
                        var
                            GenJournalLine: Record "Gen. Journal Line";
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                        begin
                            GetCurrentlySelectedLines(GenJournalLine);
                            ApprovalsMgmt.TrySendJournalLineApprovalRequests(GenJournalLine);
                        end;
                    }
                }
                group("RSMUSCancelApprovalRequest")
                {
                    Caption = 'Cancel Approval Request';
                    Image = Cancel;
                    action("RSMUSCancelApprovalRequestJournalBatch")
                    {
                        //ApplicationArea = Basic, Suite;
                        ApplicationArea = All;
                        Caption = 'Journal Batch';
                        //Enabled = CanCancelApprovalForJnlBatch OR CanCancelFlowApprovalForBatch;
                        Image = CancelApprovalRequest;
                        ToolTip = 'Cancel sending all journal lines for approval, also those that you may not see because of filters.';

                        trigger OnAction()
                        var
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                        begin
                            ApprovalsMgmt.TryCancelJournalBatchApprovalRequest(Rec);
                            SetControlAppearanceFromBatch;
                            SetControlAppearance;
                        end;
                    }
                    action("RSMUSCancelApprovalRequestJournalLine")
                    {
                        //ApplicationArea = Basic, Suite;
                        ApplicationArea = All;
                        Caption = 'Selected Journal Lines';
                        //Enabled = CanCancelApprovalForJnlLine OR CanCancelFlowApprovalForLine;
                        Image = CancelApprovalRequest;
                        ToolTip = 'Cancel sending selected journal lines for approval.';

                        trigger OnAction()
                        var
                            GenJournalLine: Record "Gen. Journal Line";
                            ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                        begin
                            GetCurrentlySelectedLines(GenJournalLine);
                            ApprovalsMgmt.TryCancelJournalLineApprovalRequests(GenJournalLine);
                        end;
                    }

                }

            }
            group("RSMUSApproval")
            {
                Caption = 'Approval';

                action("RSMUSApprove")
                {
                    ApplicationArea = All;
                    Caption = 'Approve';
                    Image = Approve;
                    ToolTip = 'Approve the requested purchase journal.';
                    //Visible = OpenApprovalEntriesExistForCurrUser;
                    Enabled = OpenApprovalEntriesExistForCurrUser;

                    trigger OnAction()
                    var
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin
                        ApprovalsMgmt.ApproveGenJournalLineRequest(Rec);
                    end;
                }
                action("RSMUSReject")
                {
                    ApplicationArea = All;
                    Caption = 'Reject';
                    Image = Reject;
                    ToolTip = 'Reject the approval request.';
                    //Visible = OpenApprovalEntriesExistForCurrUser;
                    Enabled = OpenApprovalEntriesExistForCurrUser;

                    trigger OnAction()
                    var
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin
                        ApprovalsMgmt.RejectGenJournalLineRequest(Rec);
                    end;
                }
                action("RSMUSDelegate")
                {
                    ApplicationArea = All;
                    Caption = 'Delegate';
                    ToolTip = 'Delegate the approval to a substitute approver.';
                    //Visible = OpenApprovalEntriesExistForCurrUser;
                    Enabled = OpenApprovalEntriesExistForCurrUser;

                    trigger OnAction()
                    var
                        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                    begin
                        ApprovalsMgmt.DelegateGenJournalLineRequest(Rec);
                    end;
                }
            }

        }
        addafter("A&ccount")
        {
            action("RSMUSApprovals")
            {
                AccessByPermission = TableData "Approval Entry" = R;
                ApplicationArea = All;
                Caption = 'Approvals';
                Image = Approvals;
                ToolTip = 'View a list of the records that are waiting to be approved. For example, you can see who requested the record to be approved, when it was sent, and when it is due to be approved.';

                trigger OnAction()
                var
                    GenJournalLine: Record "Gen. Journal Line";
                    ApprovalsMgmt: Codeunit "Approvals Mgmt.";
                begin
                    GetCurrentlySelectedLines(GenJournalLine);
                    ApprovalsMgmt.ShowJournalApprovalEntries(GenJournalLine);
                end;
            }

        }
    }
    var
        CurrentJnlBatchName: Code[10];

        [InDataSet]
        OpenApprovalEntriesExistForCurrUser: Boolean;
        OpenApprovalEntriesExistForCurrUserBatch: Boolean;
        OpenApprovalEntriesOnJnlBatchExist: Boolean;
        OpenApprovalEntriesOnJnlLineExist: Boolean;
        OpenApprovalEntriesOnBatchOrCurrJnlLineExist: Boolean;
        OpenApprovalEntriesOnBatchOrAnyJnlLineExist: Boolean;
        CanCancelApprovalForJnlBatch: Boolean;
        CanCancelApprovalForJnlLine: Boolean;
        CanRequestFlowApprovalForBatch: Boolean;
        CanRequestFlowApprovalForBatchAndAllLines: Boolean;
        CanRequestFlowApprovalForBatchAndCurrentLine: Boolean;
        CanCancelFlowApprovalForBatch: Boolean;
        CanCancelFlowApprovalForLine: Boolean;


    local procedure GetCurrentlySelectedLines(var GenJournalLine: Record "Gen. Journal Line"): Boolean
    begin
        CurrPage.SetSelectionFilter(GenJournalLine);
        exit(GenJournalLine.FindSet);
    end;

    local procedure SetControlAppearanceFromBatch()
    var
        GenJournalBatch: Record "Gen. Journal Batch";
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
        WorkflowWebhookManagement: Codeunit "Workflow Webhook Management";
        CanRequestFlowApprovalForAllLines: Boolean;
    begin
        if ("Journal Template Name" <> '') and ("Journal Batch Name" <> '') then
            GenJournalBatch.Get("Journal Template Name", "Journal Batch Name")
        else
            if not GenJournalBatch.Get(GetRangeMax("Journal Template Name"), CurrentJnlBatchName) then
                exit;

        CheckOpenApprovalEntries(GenJournalBatch.RecordId);

        CanCancelApprovalForJnlBatch := ApprovalsMgmt.CanCancelApprovalForRecord(GenJournalBatch.RecordId);

        WorkflowWebhookManagement.GetCanRequestAndCanCancelJournalBatch(
          GenJournalBatch, CanRequestFlowApprovalForBatch, CanCancelFlowApprovalForBatch, CanRequestFlowApprovalForAllLines);
        CanRequestFlowApprovalForBatchAndAllLines := CanRequestFlowApprovalForBatch and CanRequestFlowApprovalForAllLines;
    end;

    local procedure CheckOpenApprovalEntries(BatchRecordId: RecordID)
    var
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
    begin
        OpenApprovalEntriesExistForCurrUserBatch := ApprovalsMgmt.HasOpenApprovalEntriesForCurrentUser(BatchRecordId);

        OpenApprovalEntriesOnJnlBatchExist := ApprovalsMgmt.HasOpenApprovalEntries(BatchRecordId);

        OpenApprovalEntriesOnBatchOrAnyJnlLineExist :=
          OpenApprovalEntriesOnJnlBatchExist or
          ApprovalsMgmt.HasAnyOpenJournalLineApprovalEntries("Journal Template Name", "Journal Batch Name");
    end;

    local procedure SetControlAppearance()
    var
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
        WorkflowWebhookManagement: Codeunit "Workflow Webhook Management";
        CanRequestFlowApprovalForLine: Boolean;
    begin
        OpenApprovalEntriesExistForCurrUser :=
          OpenApprovalEntriesExistForCurrUserBatch or ApprovalsMgmt.HasOpenApprovalEntriesForCurrentUser(RecordId);

        OpenApprovalEntriesOnJnlLineExist := ApprovalsMgmt.HasOpenApprovalEntries(RecordId);
        OpenApprovalEntriesOnBatchOrCurrJnlLineExist := OpenApprovalEntriesOnJnlBatchExist or OpenApprovalEntriesOnJnlLineExist;

        CanCancelApprovalForJnlLine := ApprovalsMgmt.CanCancelApprovalForRecord(RecordId);

        WorkflowWebhookManagement.GetCanRequestAndCanCancel(RecordId, CanRequestFlowApprovalForLine, CanCancelFlowApprovalForLine);
        CanRequestFlowApprovalForBatchAndCurrentLine := CanRequestFlowApprovalForBatch and CanRequestFlowApprovalForLine;
    end;
}
/*
local procedure OpenJnl(VAR CurrentJnlBatchName: Code[]; VAR GenJnlLine: Record "Gen. Journal Line")
begin
    //CheckTemplateName(GenJnlLine.GETRANGEMAX("Journal Template Name"), CurrentJnlBatchName);
    GenJnlLine.FILTERGROUP := 2;
    GenJnlLine.SETRANGE("Journal Batch Name", CurrentJnlBatchName);
    GenJnlLine.FILTERGROUP := 0;
end;


trigger OnAfterGetCurrRecord()
var
    GenJournalBatch: Record "Gen. Journal Batch";
    WorkflowEventHandling: Codeunit "Workflow Event Handling";
    WorkflowManagement: Codeunit "Workflow Management";
begin
    SetControlAppearance;
end;

trigger OnOpenPage()
var
    ServerConfigSettingHandler: Codeunit "Server Config. Setting Handler";
    PermissionManager: Codeunit "Permission Manager";
    JnlSelected: Boolean;
begin
    if IsOpenedFromBatch then begin
        CurrentJnlBatchName := "Journal Batch Name";
        //GenJnlManagement.OpenJnl(CurrentJnlBatchName, Rec);
        OpenJnl(CurrentJnlBatchName, Rec);
        SetControlAppearanceFromBatch;
        exit;
    end;
    //GenJnlManagement.TemplateSelection(PAGE::"Payment Journal", 4, false, Rec, JnlSelected);
    //if not JnlSelected then
    //Error('');
    //GenJnlManagement.OpenJnl(CurrentJnlBatchName, Rec);
    OpenJnl(CurrentJnlBatchName, Rec);
    SetControlAppearanceFromBatch;
*/


/*
   trigger OnAfterGetRecord()
    begin
        //StyleTxt := GetOverdueDateInteractions(OverdueWarningText);
        //ShowShortcutDimCode(ShortcutDimCode);
        //HasPmtFileErr := HasPaymentFileErrors;
        //RecipientBankAccountMandatory := IsAllowPaymentExport and
          //(("Bal. Account Type" = "Bal. Account Type"::Vendor) or ("Bal. Account Type" = "Bal. Account Type"::Customer));
        //SetAMCAppearance;
    end;

    trigger OnInit()
    var
        PermissionManager: Codeunit "Permission Manager";
    begin
        //TotalBalanceVisible := true;
        //BalanceVisible := true;
        //AmountVisible := true;
    end;

    trigger OnModifyRecord(): Boolean
    begin
        //CheckForPmtJnlErrors;
    end;

    trigger OnNewRecord(BelowxRec: Boolean)
    begin
        //HasPmtFileErr := false;
        //UpdateBalance;
        //EnableApplyEntriesAction;
        //SetUpNewLine(xRec, Balance, BelowxRec);
        //Clear(ShortcutDimCode);
        if not VoidWarningDisplayed then begin
            GenJnlTemplate.Get("Journal Template Name");
            if not GenJnlTemplate."Force Doc. Balance" then
                Message(CheckCannotVoidMsg);
            VoidWarningDisplayed := true;
        end;
        SetAMCAppearance;
    end;
*/



/*
action("RSMUSCreateFlow")
{
    ApplicationArea = Basic, Suite;
    Caption = 'Create a Flow';
    Image = Flow;
    ToolTip = 'Create a new Flow from a list of relevant Flow templates.';
    Visible = IsSaaS;

    trigger OnAction()
    var
        FlowServiceManagement: Codeunit "Flow Service Management";
        FlowTemplateSelector: Page "Flow Template Selector";
    begin
        // Opens page 6400 where the user can use filtered templates to create new flows.
        FlowTemplateSelector.SetSearchText(FlowServiceManagement.GetJournalTemplateFilter);
        FlowTemplateSelector.Run;
    end;
}
action("RSMUSSeeFlows")
{
    ApplicationArea = Basic, Suite;
    Caption = 'See my Flows';
    Image = Flow;
    RunObject = Page "Flow Selector";
    ToolTip = 'View and configure Flows that you created.';
}
}

group("RSMUSWorkflow")
{
Caption = 'Workflow';
action("RSMUSCreateApprovalWorkflow")
{
    ApplicationArea = Suite;
    Caption = 'Create Approval Workflow';
    Enabled = NOT EnabledApprovalWorkflowsExist;
    Image = CreateWorkflow;
    ToolTip = 'Set up an approval workflow for payment journal lines, by going through a few pages that will guide you.';

    trigger OnAction()
    var
        TempApprovalWorkflowWizard: Record "Approval Workflow Wizard" temporary;
    begin
        TempApprovalWorkflowWizard."Journal Batch Name" := "Journal Batch Name";
        TempApprovalWorkflowWizard."Journal Template Name" := "Journal Template Name";
        TempApprovalWorkflowWizard."For All Batches" := false;
        TempApprovalWorkflowWizard.Insert;

        PAGE.RunModal(PAGE::"Pmt. App. Workflow Setup Wzrd.", TempApprovalWorkflowWizard);
    end;
}
action("RSMUSManageApprovalWorkflows")
{
    ApplicationArea = Suite;
    Caption = 'Manage Approval Workflows';
    Enabled = EnabledApprovalWorkflowsExist;
    Image = WorkflowSetup;
    ToolTip = 'View or edit existing approval workflows for payment journal lines.';

    trigger OnAction()
    var
        WorkflowManagement: Codeunit "Workflow Management";
    begin
        WorkflowManagement.NavigateToWorkflows(DATABASE::"Gen. Journal Line", EventFilter);
    end;
}
}
group("RSMUSApproval")
{
Caption = 'Approval';
action("RSMUSApprove")
{
    ApplicationArea = All;
    Caption = 'Approve';
    Image = Approve;
    Promoted = true;
    PromotedCategory = Category6;
    PromotedIsBig = true;
    ToolTip = 'Approve the requested changes.';
    Visible = OpenApprovalEntriesExistForCurrUser;

    trigger OnAction()
    var
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
    begin
        ApprovalsMgmt.ApproveGenJournalLineRequest(Rec);
    end;
}
action("RSMUSReject")
{
    ApplicationArea = All;
    Caption = 'Reject';
    Image = Reject;
    Promoted = true;
    PromotedCategory = Category6;
    PromotedIsBig = true;
    ToolTip = 'Reject the approval request.';
    Visible = OpenApprovalEntriesExistForCurrUser;

    trigger OnAction()
    var
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
    begin
        ApprovalsMgmt.RejectGenJournalLineRequest(Rec);
    end;
}
action("RSMUSDelegate")
{
    ApplicationArea = All;
    Caption = 'Delegate';
    Image = Delegate;
    Promoted = true;
    PromotedCategory = Category6;
    ToolTip = 'Delegate the approval to a substitute approver.';
    Visible = OpenApprovalEntriesExistForCurrUser;

    trigger OnAction()
    var
        ApprovalsMgmt: Codeunit "Approvals Mgmt.";
    begin
        ApprovalsMgmt.DelegateGenJournalLineRequest(Rec);
    end;
}
}
*/
