/*
RSM0002 GHL 03/27/2020 General Enhancements - G004
  Added control RSMUSIncludeOnSO.
*/
pageextension 50115 "RSMUSChartofAccounts" extends "Chart of Accounts"
{
    layout
    {
        modify(Name)
        {
            Visible = false;
        }
        addafter("No.")
        {
            field(RSMUSName61420; Name)
            {
                ApplicationArea = All;
            }
        }
        modify("Net Change")
        {
            Visible = false;
        }
        //RSM0001>>
        addafter(Balance)
        {
            field(RSMUSIncluded; RSMUSIncluded)
            {
                ApplicationArea = all;
            }
            // RSM0002 >>
            field(RSMUSIncludeOnSO; RSMUSIncludeOnSO)
            {
                ApplicationArea = All;
            }
            // RSM0002 <<
        }
        //RSM0001<<
    }
    actions
    {
    }
}
