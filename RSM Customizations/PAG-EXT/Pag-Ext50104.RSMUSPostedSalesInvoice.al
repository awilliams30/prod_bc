pageextension 50104 "RSMUSPostedSalesInvoice" extends "Posted Sales Invoice"
{
    layout
    {
        addlast(General)
        {
            field("RSMUSWITS Sales Order No."; "RSMUSWITS SO No.")
            {
                ApplicationArea = all;
            }
        }
        modify("Order No.")
        {
            Caption = 'Sales Order No.';
        }
        modify("External Document No.")
        {
            Caption = 'Customer PO No.';
        }
    }

    actions
    {
        modify(Print)
        {
            Caption = 'Print Standard Layout';
        }

        addafter(Email)
        {
            action(RSMUSPrintFacebookLayout)
            {
                ApplicationArea = all;
                Caption = 'Print Facebook Invoice';
                //RunObject = report RSMUSFacebookSalesInvoice;

                Image = Print;
                //Promoted = true;
                //PromotedOnly = true;
                //PromotedCategory = Process;
                trigger OnAction()
                var
                    SalesInvHeader: Record "Sales Invoice Header";
                begin

                    CurrPage.SetSelectionFilter(SalesInvHeader);

                    Report.RunModal(50104, true, false, SalesInvHeader);
                end;
            }
        }
    }
}