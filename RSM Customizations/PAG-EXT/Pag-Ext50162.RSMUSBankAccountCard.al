pageextension 50162 "RSMUSBankAccountCard" extends "Bank Account Card"
{
    //RSM0002 MP 03/23/2021: Wells Fargo Check file export last check no.

    layout
    {
        addafter("Transit No.2")
        {
            field(RSMUSWellsFargoBankAcct; RSMUSWellsFargoBankAcct)
            {
                ApplicationArea = all;
            }
        }
        //RSM0002>>
        addafter("Last Check No.")
        {
            field(RSMUSWellsFargoCheckNo; RSMUSWellsFargoCheckNo)
            {
                ApplicationArea = all;
                ToolTip = 'Specifies the last Wells Fargo Check No.';
                // Visible = WFCheckNoVisible;
                // Editable = WFCheckNoVisible;

            }
        }
        //RSM0001<<
    }

    trigger OnAfterGetCurrRecord()
    begin
        WFCheckNoVisible := Rec.RSMUSWellsFargoBankAcct;
        CurrPage.Update(true);
    end;

    trigger OnOpenPage()
    begin
        WFCheckNoVisible := false;
    end;

    var
        [InDataSet]
        WFCheckNoVisible: Boolean;
}