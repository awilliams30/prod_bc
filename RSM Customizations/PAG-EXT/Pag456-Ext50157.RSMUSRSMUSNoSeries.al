/*
    RSM0000 MP 05/29/2020 G008: New page extension to allow posting no series selection
*/
pageextension 50157 "RSMUSNoSeries" extends "No. Series" // 456
{
    layout
    {
        addbefore("Default Nos.")
        {
            field("Posting Code";"Posting Code")
            {
                ApplicationArea = all;
            }
        }
    }
}