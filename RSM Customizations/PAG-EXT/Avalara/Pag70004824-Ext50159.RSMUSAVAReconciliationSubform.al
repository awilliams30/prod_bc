/*
    RSM0001 MP 10/16/2020 AvaTaxReconciliationImportExport: Added process to allow for importing and exporting Ava Reconciliation Records
*/

pageextension 50159 "RSMUSAVAReconciliationSubform" extends "AVA Reconciliation Subform" //70004824
{
    //RSM0001 >>
    var
        ExcelBuffer: Record "Excel Buffer";
        Rows: Integer;
        Columns: Integer;
        Filename: Text;
        FileMgmt: Codeunit "File Management";
        ExcelFile: File;
        Instr: InStream;
        Sheetname: Text;
        FileUploaded: Boolean;
        RowNo: Integer;
        ColNo: Integer;
        AvaReconciliation: Record "AVA Reconciliation";
        AvaReconciliationTemp: Record "AVA Reconciliation" temporary;


    procedure ImportAvaReconciliationExcel()
    var
        PK1: Text;
        PK2: Text;
        PK3: Text;
    begin
        clear(Filename);
        Clear(Sheetname);

        ExcelBuffer.DeleteAll();
        Rows := 0;
        Columns := 0;
        FileUploaded := UploadIntoStream('Select File to Upload', '', '', Filename, Instr);

        if Filename <> '' then
            Sheetname := ExcelBuffer.SelectSheetsNameStream(Instr)
        else
            exit;


        ExcelBuffer.Reset;
        ExcelBuffer.OpenBookStream(Instr, Sheetname);
        ExcelBuffer.ReadSheet();

        Commit();
        ExcelBuffer.Reset();
        ExcelBuffer.SetRange("Column No.", 1);
        if ExcelBuffer.FindFirst() then
            repeat
                Rows := Rows + 1;
            until ExcelBuffer.Next() = 0;

        for RowNo := 2 to Rows do begin
            Clear(AvaReconciliationTemp);
            PK1 := GetValueAtIndex(RowNo, 1);
            PK2 := GetValueAtIndex(RowNo, 2);
            PK3 := GetValueAtIndex(RowNo, 3);

            Evaluate(AvaReconciliationTemp.Module, PK1);
            Evaluate(AvaReconciliationTemp."Document Type", PK2);
            Evaluate(AvaReconciliationTemp."Document No.", PK3);

            if AvaReconciliation.Get(AvaReconciliationTemp.Module, AvaReconciliationTemp."Document Type", AvaReconciliationTemp."Document No.") then begin
                SetNonPrimaryKeyFields();
                AvaReconciliation.Modify(true);
            end else begin
                AvaReconciliation.Init();
                AvaReconciliation.Validate(Module, AvaReconciliationTemp.Module);
                AvaReconciliation.Validate("Document Type", AvaReconciliationTemp."Document Type");
                AvaReconciliation.Validate("Document No.", AvaReconciliationTemp."Document No.");
                SetNonPrimaryKeyFields();

                AvaReconciliation.Insert(true);
            end;

        end;
        Message('%1 Rows Imported Successfully!!', Rows - 1);
    end;

    local procedure GetValueAtIndex(RowNo: Integer; ColNo: Integer): Text
    var
    begin
        ExcelBuffer.Reset();
        IF ExcelBuffer.Get(RowNo, ColNo) then
            exit(ExcelBuffer."Cell Value as Text");
    end;

    procedure ExportAvaReconciliationExcel()
    begin
        ExportHeaderAvaReconciliation();
        //CurrPage.subSalesRecon.Page.GetSubPageRecFilters(AvaReconciliation);
        AvaReconciliation := Rec;
        if AvaReconciliation.Find('-') then begin
            repeat
                ExcelBuffer.NewRow();
                ExcelBuffer.AddColumn(Format(AvaReconciliation.Module), false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(Format(AvaReconciliation."Document Type"), false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation."Document No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Number);
                ExcelBuffer.AddColumn(AvaReconciliation."Document Date", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation."Customer No.", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation."Customer Name", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation.Amount, false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation."Ava Taxable Amount", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(AvaReconciliation."Ava Tax Amount", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Number);
                ExcelBuffer.AddColumn(AvaReconciliation."Ava Exempted Amount", false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
                ExcelBuffer.AddColumn(format(AvaReconciliation.Status), false, '', false, false, false, '', ExcelBuffer."Cell Type"::Text);
            until AvaReconciliation.next = 0;
            ExcelBuffer.CreateNewBook('AvaTax Reconciliation');
            ExcelBuffer.WriteSheet('AvaTax Reconciliation', CompanyName(), UserId());
            ExcelBuffer.CloseBook();
            ExcelBuffer.OpenExcel();
        end else
            Message('Nothing to export');
    end;

    local procedure ExportHeaderAvaReconciliation();
    begin
        ExcelBuffer.Reset();
        ExcelBuffer.DeleteAll();
        ExcelBuffer.Init();
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption(Module), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Document Type"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Document No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Document Date"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Customer No."), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Customer Name"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption(Amount), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Ava Taxable Amount"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Ava Tax Amount"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption("Ava Exempted Amount"), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
        ExcelBuffer.AddColumn(AvaReconciliation.FieldCaption(Status), false, '', true, false, false, '', ExcelBuffer."Cell Type"::Text);
    end;

    procedure SetNonPrimaryKeyFields()
    begin
        //start at 4
        //    Evaluate(AvaReconciliation.,GetValueAtIndex(RowNo,));
        // AvaReconciliation.Validate();

        //Validate("Posting Date", CalcDate('1D', AvaReconciliation."Document Date"));

        Evaluate(AvaReconciliation."Document Date", GetValueAtIndex(RowNo, 4));
        if AvaReconciliation."Document Date" <> 0D then
            AvaReconciliation.Validate("Document Date", CalcDate('1D', AvaReconciliation."Document Date"));
        //AvaReconciliation.Validate("Document Date");
        Evaluate(AvaReconciliation."Customer No.", GetValueAtIndex(RowNo, 5));
        AvaReconciliation.Validate("Customer No.");
        Evaluate(AvaReconciliation."Customer Name", GetValueAtIndex(RowNo, 6));
        AvaReconciliation.Validate("Customer Name");
        Evaluate(AvaReconciliation.Amount, GetValueAtIndex(RowNo, 7));
        AvaReconciliation.Validate(Amount);
        Evaluate(AvaReconciliation."Ava Taxable Amount", GetValueAtIndex(RowNo, 8));
        AvaReconciliation.Validate("Ava Taxable Amount");
        Evaluate(AvaReconciliation."Ava Tax Amount", GetValueAtIndex(RowNo, 9));
        AvaReconciliation.Validate("Ava Tax Amount");
        Evaluate(AvaReconciliation."Ava Exempted Amount", GetValueAtIndex(RowNo, 10));
        AvaReconciliation.Validate("Ava Exempted Amount");
        Evaluate(AvaReconciliation.Status, GetValueAtIndex(RowNo, 11));
        AvaReconciliation.Validate(Status);
    end;
    //RSM0001 <<

}