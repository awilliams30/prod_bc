pageextension 50148 "RSMUSGeneralPostingSetup" extends "General Posting Setup"
{
    layout
    {
        modify("COGS Account (Interim)")
        {
            ApplicationArea = All;
        }

        modify("Invt. Accrual Acc. (Interim)")
        {
            ApplicationArea = All;
        }
    }
}