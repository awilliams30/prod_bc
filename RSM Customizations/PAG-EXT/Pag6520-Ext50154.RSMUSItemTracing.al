/*
RSM0001 GHL 03/19/2020 Generic Enhancements G003
    Added field(s) RSMUSVariant Description 2
*/

pageextension 50154 "RSMUSItemTracing" extends "Item Tracing" // 6520
{
    layout
    {
        addafter(Description)
        {
            field("RSMUSVariant Description 2"; "RSMUSVariant Description 2")
            {
                ApplicationArea = All;
            }
        }
    }
}