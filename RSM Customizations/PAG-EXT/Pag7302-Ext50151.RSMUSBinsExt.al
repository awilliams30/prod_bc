pageextension 50151 "RSMUSBinsExt" extends Bins // 7302
{
    layout
    {
        addafter(Dedicated)
        {
            field("RSMUSExclude from Sales Report"; "RSMUSExclude from Sales Report")
            {
                ApplicationArea = all;
            }
        }
    }
}