pageextension 50155 "RSMUSBinList" extends "Bin List" // 7303
{
    layout
    {
        addafter(Dedicated)
        {
            field("RSMUSExclude from Sales Report"; "RSMUSExclude from Sales Report")
            {
                ApplicationArea = all;
            }
        }
    }
}