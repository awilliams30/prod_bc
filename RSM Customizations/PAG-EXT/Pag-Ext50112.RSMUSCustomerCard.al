pageextension 50112 "RSMUSCustomerCard" extends "Customer Card"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addafter(SaveAsTemplate)
        {
            action("RSMUSCopyCustomer")
            {
                ApplicationArea = all;
                Caption = 'Copy Customer';
                Image = ChangeCustomer;

                trigger OnAction()
                var
                    lCustomer: Record Customer;
                    length: Integer;
                    lDefaultDimensionSource: Record "Default Dimension";
                    lDefaultDimensionDestination: Record "Default Dimension";
                    confirm: Boolean;
                    lCustomerCard: Page "Customer Card";
                begin
                    length := StrLen('RS-' + Rec."No.");
                    if length > 20 then begin
                        Error('Customer No. exceeded length.');
                    end else begin
                        lCustomer.Init();
                        lCustomer.Copy(Rec, false);
                        lCustomer.Validate("No.", 'RS-' + Rec."No.");
                        lCustomer.Insert();
                        lDefaultDimensionSource.Reset();
                        lDefaultDimensionSource.SetRange("Table ID", 18);
                        lDefaultDimensionSource.SetRange("No.", Rec."No.");
                        IF lDefaultDimensionSource.FindSet() then begin
                            repeat
                                lDefaultDimensionDestination.Copy(lDefaultDimensionSource, false);
                                lDefaultDimensionDestination.Validate("No.", lCustomer."No.");
                                lDefaultDimensionDestination.Insert();
                            until lDefaultDimensionSource.Next() = 0;
                        end;
                        confirm := Confirm('Customer has been copied. Would you like to navigate to their card?');
                        if confirm then begin
                            Clear(lCustomerCard);
                            lCustomerCard.SetTableView(lCustomer);
                            lCustomerCard.SetRecord(lCustomer);
                            lCustomerCard.Run();
                        end;
                    end;
                end;
            }
        }
    }
}